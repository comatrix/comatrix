# CoMatrix Testbed Setup

#### Table of contents

1. [CoMatrix Architecture and Testbed Overview](#comatrix-architecture-and-testbed-overview)
2. [Testbed Hardware](#testbed-hardware)
3. [Testbed Software/Operating Systems](#testbed-softwareoperating-systems)
4. [Raspberry Pi 6LoWPAN Setup](#raspberry-pi-6lowpan-setup)
    * [Install Raspbian Buster on Raspberry Pi 3B+](#install-raspbian-buster-on-raspberry-pi-3b)
    * [Preparation of Raspbian Buster](#preparation-of-raspbian-buster)
    * [Enable support for IEEE 802.15.4/LoWPAN devices ](#enable-support-for-ieee-802154lowpan-devices-)
        - [Installation of Linux WPAN tools](#installation-of-linux-wpan-tools)
        - [MANUAL WPAN interface configuration](#manual-wpan-interface-configuration)
        - [PERSISTENT wpan0 and lowpan0 configuration](#persistent-wpan0-and-lowpan0-configuration)
5. [RIOT-OS and SAMR21-xpro](#riot-os-and-samr21-xpro)
6. [Verify connectivity over 6LoWPAN between Raspi and SAMR21-xpro](#verify-connectivity-over-6lowpan-between-raspi-and-samr21-xpro)
    * [Ping test from Raspi to SAMR21-xpro](#ping-test-from-raspi-to-samr21-xpro)
    * [Ping test from SAMR21-xpro to Raspi](#ping-test-from-samr21-xpro-to-raspi)
    * [Monitoring 802.15.4 traffic with Nordic nRF52840-Dongle (PCA10059)](#monitoring-802154-traffic-with-nordic-nrf52840-dongle-pca10059)
    * [Possible Problems with 6LoWPAN, SAMR21-xpro and Raspberry Pi (with Openlabs 802.15.4 radio)](#possible-problems-with-6lowpan-samr21-xpro-and-raspberry-pi-with-openlabs-802154-radio)
7. [Test CoAP communication between Raspi and SAMR21-xpro](#test-coap-communication-between-raspi-and-samr21-xpro)
8. [Run Matrix Synapse Homeserver as Docker Container with SQLite DB](#run-matrix-synapse-homeserver-as-docker-container-with-sqlite-db)
9. [Run Matrix Synapse Homeserver as Docker Container with Postgres DB](#run-matrix-synapse-homeserver-as-docker-container-with-postgres-db)
10. [User and room creation at Matrix Synapse homeserver](#user-and-room-creation-at-matrix-synapse-homeserver)
11. [CoMatrix gateway setup](#comatrix-gateway-setup)
12. [CoMatrix client setup](#comatrix-client-setup)

## CoMatrix Architecture and Testbed Overview

![CoMatrix Architecture & Testbed](Architecture/CoMatrix_Architecture_Final.png "CoMatrix Architecture & Testbed")

## Testbed Hardware

- CoMatrix Gateway:
  - Raspberry Pi 3B(+)
      - https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/
  - OpenLabs Raspberry Pi 802.15.4 radio module
    - https://openlabs.co/store/Raspberry-Pi-802.15.4-radio
    - **CAUTION**: In our tests the module was not able to successfully communicate when used with a Raspberry Pi 4 and 
      current Raspbian/Raspberry Pi OS (i.e. `Linux raspberrypi 5.10.17-v7+`)! We recommend to use a Raspberry Pi 3B(+).
- CoMatrix Client:
    - Atmel SAM R21 Xplained Pro Evaluation Kit (SAMR21-xpro) microcontroller
        - https://www.microchip.com/developmenttools/ProductDetails/ATSAMR21-XPRO
    - Temperature sensor "DS18S20+"
        - https://at.farnell.com/maxim-integrated-products/ds18s20/digital-thermometer-18s20-to92/dp/2519401?st=Maxim%20DS18S20
- Matrix Synapse homeserver:
    - Raspberry Pi 4
- A laptop (running Matrix Element client)
    - OPTIONAL: Nordic nRF52840 Dongle for monitoring/sniffing of 802.15.4 network traffic
        - https://www.nordicsemi.com/Software-and-tools/Development-Kits/nRF52840-Dongle

![Real Testbed](resources/SAMR21xpro-Raspberry_Pi_3B+_1.png "Real Testbed")

## Testbed Software/Operating Systems

* Raspi
  * Raspberry Pi OS Lite - Buster (Linux raspberrypi 5.10.17-v7+)
* SAMR21-xpro
  *  RIOT OS (Release-2021.04, tag 46bc55f514)
    * https://github.com/RIOT-OS/RIOT/releases/tag/2021.04
* Laptop 
  * Ubuntu 18.04 (5.4.0-70-generic #78~18.04.1-Ubuntu)

## Raspberry Pi 6LoWPAN Setup

### Install Raspbian Buster on Raspberry Pi 3B+

* Use Raspberry Pi Imager v1.5 on Ubuntu 18.04 Linux
* Download Raspbian/Raspberry Pi OS from https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-03-25/2021-03-04-raspios-buster-armhf-lite.zip)

### Preparation of Raspbian Buster

Raspi needs to be connected to the LAN via an Ethernet cable. For the first steps the Raspi is connected via HDMI to a monitor. After enabling SSH it is possible to connect via SSH/terminal from the Ubuntu Laptop to the Raspi.

* Change default password of `pi` user:
  * Default credentials: username: `pi`, password: `raspberry`
  
```shell
passwd
Changed password to comatrix
```
  
* (Optionally) Change keyboard setting to `German` and reboot:
    
```shell
sudo dpkg-reconfigure keyboard-configuration
```

* Configure static IP settings depending on your LAN setup:

```shell
sudo nano /etc/network/interfaces
auto eth0
iface eth0 inet static
address 192.168.1.106
netmask 255.255.255.248
gateway 192.168.1.105
dns-nameservers 192.168.1.1
```

* If eth0 still does not show up in `ifconfig` here is the magic:
  
```shell
sudo ifdown eth0 && sudo ifup eth0
```

* Activate and start SSH on Raspi:
  
```shell
sudo systemctl start ssh
sudo systemctl enable ssh
```
        
* Connect to the Raspi via SSH from ubuntu terminal:
  
```shell
ssh pi@192.168.1.106
```

* Upgrade of Raspbian:

```shell
sudo apt-get update && sudo apt-get upgrade
```

### Enable support for IEEE 802.15.4/LoWPAN devices 

* Plug the *OpenLabs 802.15.4 radio* module directly onto pins 15-26 of the Raspi's P1 header (in the middle):

OpenLabs 802.15.4 radio module             |  Module connected to Raspi
:-------------------------:|:-------------------------:
![OpenLabs 802.15.4 radio module](resources/raspberry-pi-802.15.4-radio_module.jpg "OpenLabs 802.15.4 radio module")  |  ![Connected to Raspberry Pi](resources/raspberry-pi-802.15.4-radio_connected.jpg "Connected to Raspberry Pi")
Image source: https://openlabs.co/store/Raspberry-Pi-802.15.4-radio | Image source: https://openlabs.co/store/Raspberry-Pi-802.15.4-radio

* There is an existing overlay for this transceiver shipped with Raspbian, should be in `/boot/overlays/at86rf233-overlay.dtb` or `/boot/overlays/at86rf233.dtbo`:

```shell
ls /boot/overlays/at86rf233.dtbo
```
        
* Enable transceiver by modifying the `/boot/config.txt` file and reboot:
   
```shell
sudo nano /boot/config.txt
# enable OpenLabs 802.15.4 radio module
dtoverlay=at86rf233, speed=3000000
```
  
#### Installation of Linux WPAN tools
  
**Source/Links:** 

- https://github.com/RIOT-OS/RIOT/wiki/How-to-install-6LoWPAN-Linux-Kernel-on-Raspberry-Pi
- https://github.com/RIOT-Makers/wpan-raspbian/wiki/Create-a-generic-Raspbian-image-with-6LoWPAN-support
- https://github.com/RIOT-Makers/wpan-raspbian/wiki/Spice-up-Raspbian-for-the-IoT
  
___
      
* First install `git` and clone the wpan-tools repo:
        
```shell
sudo apt install git
sudo mkdir /opt/src
sudo chown pi /opt/src
cd /opt/src
git clone https://github.com/linux-wpan/wpan-tools 
```
        
* Install some required packages before building the wpan-tools:
            
```shell
sudo apt install dh-autoreconf libnl-3-dev libnl-genl-3-dev
```      
        
* Configure, build and install wpan-tools:
            
```shell
cd /opt/src/wpan-tools
./autogen.sh
./configure CFLAGS='-g -O0' --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib
make
sudo make install
```
       
* Verification/test of wpan-tools installation:

```shell
iwpan dev
```
    
#### MANUAL WPAN interface configuration

Sources/Links::

- https://linux-wpan.org/documentation.html
- https://www.linuxjournal.com/content/low-power-wireless-6lowpan-ieee802154-and-raspberry-pi
- http://morschi.com/2017/04/05/hands-on-riot-6lowpan/

___

* Configure the default PAN ID of RIOT-OS/SAMR21-xpro: `0x23`
* Configure the default channel ID of RIOT-OS/SAMR21-xpro: `26`

```shell
ip link set lowpan0 down
ip link set wpan0 down
iwpan phy phy0 set channel 0 26 # same network ID and channel as RIOT default
iwpan dev wpan0 set pan_id 0x23 # same network ID and channel as RIOT default
ip link add link wpan0 name lowpan0 type lowpan
ip link set wpan0 up
ip link set lowpan0 up
```

* Afterwards the `lowpan0` interface on Raspi has a link-local IPv6 address:

```shell
root@raspberrypi:~# ip a
1: lo: [...]
2: eth0: [...]
3: wlan0: [...]
4: wpan0: <BROADCAST,NOARP,UP,LOWER_UP> mtu 123 qdisc pfifo_fast state UNKNOWN group default qlen 300
    link/ieee802.15.4 36:3a:07:cc:65:41:2b:31 brd ff:ff:ff:ff:ff:ff:ff:ff
5: lowpan0@wpan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1280 qdisc noqueue state UNKNOWN group default qlen 1000
    link/6lowpan 36:3a:07:cc:65:41:2b:31 brd ff:ff:ff:ff:ff:ff:ff:ff
    inet6 fe80::343a:7cc:6541:2b31/64 scope link 
       valid_lft forever preferred_lft forever
```

**NOTE/CAUTION:** Every reboot changes the link-local IPv6 address of the `lowpan0` interface on the Raspi! 
See [this section](#persistent-wpan0-and-lowpan0-config) to configure a persistent link-local address.

There are also scripts available to create/delete a lowpan interface (or to create/delete a monitor interface): https://github.com/RIOT-Makers/wpan-raspbian

#### PERSISTENT wpan0 and lowpan0 configuration
  
with static long hardware address and static link-local IPv6 address (to avoid a random link-local IPv6 address on each reboot)
        
**Sources/Links:**

- https://github.com/RIOT-Makers/wpan-raspbian/wiki/Spice-up-Raspbian-for-the-IoT
- https://github.com/RIOT-Makers/wpan-raspbian
- https://jan.newmarch.name/IoT/LinuxJournal/Routing/

___

* Clone riot-makers/wpan-raspbian git repo:
        
```shell
cd /opt/src
git clone https://github.com/riot-makers/wpan-raspbian
cd wpan-raspbian
```
        
* Copy some helper (shell) scripts to a well-known location:

```shell
sudo cp -r usr/local/sbin/* /usr/local/sbin/.
sudo chmod +x /usr/local/sbin/*
```

- Afterwards copy files for systemd integration:

```shell
sudo cp etc/default/lowpan /etc/default/.
sudo cp etc/systemd/system/lowpan.service /etc/systemd/system/.
```

- Modify channel id and PAN id in `/etc/default/lowpan` as needed:
    
```shell
sudo nano /etc/default/lowpan 
CHN="13"
PAN="0xbeef"
# set MAC to "" for random mac/hw address
MAC="02:0:0:0:0:0:0:1"
# set IP6 to "" if not required; note: set a prefix length
#IP6="fdaa:bb:cc:dd::1/64"
# Set ack requests (Only enable if all devices on your PAN support acks)
ACKREQ=0
```

`MAC="02:0:0:0:0:0:0:1"` was taken from https://jan.newmarch.name/IoT/LinuxJournal/Routing/, this results in a 
link-local address of `fe80::1` for the Raspi.
        
* Enable `lowpan.service` autostart, start 6LoWPAN service and verification:

```shell
sudo systemctl enable lowpan.service
sudo systemctl start lowpan.service
ifconfig
lowpan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1280
    inet6 fe80::1  prefixlen 64  scopeid 0x20<link>
    unspec 02-00-00-00-00-00-00-01-00-00-00-00-00-00-00-00  txqueuelen 1000  (UNSPEC)
    RX packets 0  bytes 0 (0.0 B)
    RX errors 0  dropped 0  overruns 0  frame 0
    TX packets 19  bytes 2114 (2.0 KiB)
    TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

wpan0: flags=195<UP,BROADCAST,RUNNING,NOARP>  mtu 123
        unspec 02-00-00-00-00-00-00-01-00-00-00-00-00-00-00-00  txqueuelen 300  (UNSPEC)
        RX packets 72  bytes 2016 (1.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 74  bytes 5320 (5.1 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

## RIOT-OS and SAMR21-xpro

**Sources/Links:**

- RIOT-OS Setup: https://wiki.elvis.science/index.php?title=Riot-OS_Setup
- SAMR21-xpro User Guide: ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42243-SAMR21-Xplained-Pro_User-Guide.pdf
- SAMR21-xpro Data Sheet: https://ww1.microchip.com/downloads/en/DeviceDoc/SAM-R21_Datasheet.pdf

___

Operating system used on laptop/computer: Debian/Ubuntu Linux

* Install necessary packages:

```shell
sudo apt install git openocd gcc-multilib build-essential python-serial libudev-dev
```

* Download _GNU Arm Embedded Toolchain_: https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm
    * CoMatrix was tested with: `GNU Arm Embedded Toolchain 9-2020-q2-update`

* Unzip the downloaded file:

```shell
tar -xjvf gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2
```

* Add toolchain to PATH:

```shell
export PATH=$PATH:<path to unzipped folder>/gcc-arm-none-eabi-9-2020-q2-update/bin
``` 

* Add toolchain permanently to PATH (otherwise it's necessary to do it manually after every restart):

`vim ~/.profile` and/or `vim ~/.bashrc`

* Add the following line to the bottom and save:

```shell
export PATH=$PATH:<path to unzipped folder>/gcc-arm-none-eabi-9-2020-q2-update/bin
```

* Apply changes for current shell:

```shell
source ~/.profile
source ~/.bashrc
```

* Download RIOT-OS:

```shell
git clone https://github.com/RIOT-OS/RIOT.git
```

* Set the rights for your user to be able to flash applications on the board:

```shell
sudo usermod -a -G dialout <username>
```

* Connect the SAMR21-xpro board to your laptop/computer via micro USB cable (USB EDBG interface):

![Connect the SAMR21-xpro board to your laptop/computer](resources/Samr21EDBGconnect.jpg "Connect the SAMR21-xpro board to your laptop/computer")

* Test building and flashing an application (to SAMR21-xpro) with the provided `hello-world` example:

```shell
cd RIOT/examples/hello-world
make BOARD=samr21-xpro
make BOARD=samr21-xpro flash
```

* Connect to SAMR21-xpro via term:

```shell
make BOARD=samr21-xpro term
```

* Or via `pyterm` (which is shipped with RIOT-OS):

```shell
sudo pip3 install pyserial
cd RIOT/dist/tools/pyterm
./pyterm -p /dev/ttyACM0 
```

* To find out the correct `tty`:

```shell
$ cd RIOT/examples/hello-world
$ make list-ttys
/sys/bus/usb/devices/1-3: Atmel Corp. EDBG CMSIS-DAP, serial: 'ATMLXXXXXXX80000XXX1', tty(s): ttyACM0
/sys/bus/usb/devices/1-2: Atmel Corp. EDBG CMSIS-DAP, serial: 'ATMLXXXXXXX80000XXX2', tty(s): ttyACM1
```

* If you have multiple boards connected to your computer it is necessary to provide the serial during flashing:

```shell
BOARD=samr21-xpro SERIAL="ATMLXXXXXXX80000XXX1" make flash
```

## Verify connectivity over 6LoWPAN between Raspi and SAMR21-xpro

* Use gnrc_networking example on Ubuntu and build it for the SAMR21-xpro board, flash the firmware to the SAMR21-xpro and connect via `pyterm`:
    
```shell
cd RIOT/examples/gnrc_networking
make BOARD=samr21-xpro
make BOARD=samr21-xpro flash
make BOARD=samr21-xpro term
```
  
* Use `ifconfig` to determine the interface id of SAMR21-xpro (`7` in our case):
  
```
ifconfig
# Iface  7  HWaddr: 57:98  Channel: 26  Page: 0  NID: 0x23  PHY: O-QPSK 
[...]
#           Long HWaddr: E2:F9:02:0F:36:26:A2:68 
[...]
#           inet6 addr: fe80::e0f9:20f:3626:a268  scope: link  VAL
```

* All `ifconfig` config options can be shown with `ifconfig <interface id> set`.
  
* RIOT-OS default 802.15.4 channel is `26` and default  PAN ID `0x23`. We kept the defaults, if you change them, make sure to change them in the SAMR21-xpro configuration aswell:
    * [optional] Change the channel to `13` and the PAN ID to `0xbeef`:
    
```shell
ifconfig 7 set pan_id 0xbeef
ifconfig 7 set channel 13
```

### Ping test from Raspi to SAMR21-xpro

* Multicast ping test through interface `lowpan0` to group `ff02::1` (all nodes in the link-local):

```shell
ping ff02::1%lowpan0        
PING ff02::1%lowpan0(ff02::1%lowpan0) 56 data bytes
64 bytes from fe80::1%lowpan0: icmp_seq=1 ttl=64 time=0.195 ms
64 bytes from fe80::e0f9:20f:3626:a268%lowpan0: icmp_seq=1 ttl=64 time=10.4 ms (DUP!)
```

* Unicast ping:

```shell
ping fe80::e0f9:20f:3626:a268%lowpan0
ping fe80::e0f9:20f:3626:a268%lowpan0
PING fe80::e0f9:20f:3626:a268%lowpan0(fe80::e0f9:20f:3626:a268%lowpan0) 56 data bytes
64 bytes from fe80::e0f9:20f:3626:a268%lowpan0: icmp_seq=1 ttl=64 time=23.0 ms
[...]
```

### Ping test from SAMR21-xpro to Raspi

* Multicast ping test through interface `7` to group `ff02::1`:

```
# ping6 ff02::1%7
# 12 bytes from fe80::1%7: icmp_seq=0 ttl=64 rssi=-61 dBm time=8.309 ms
# 12 bytes from fe80::1%7: icmp_seq=1 ttl=64 rssi=-61 dBm time=6.124 ms
# 12 bytes from fe80::1%7: icmp_seq=2 ttl=64 rssi=-59 dBm time=6.461 ms
# 
# --- ff02::1 PING statistics ---
# 3 packets transmitted, 3 packets received, 0% packet loss
# round-trip min/avg/max = 6.124/6.964/8.309 ms
```

* Unicast ping from SAMR21-xpro to Raspi 6LoWPAN interface:

```
# : ping6 fe80::1%7
# 12 bytes from fe80::1%7: icmp_seq=0 ttl=64 rssi=-61 dBm time=8.467 ms
# 12 bytes from fe80::1%7: icmp_seq=1 ttl=64 rssi=-61 dBm time=7.966 ms
```

### Monitoring 802.15.4 traffic with Nordic nRF52840-Dongle (PCA10059)

We used a Nordic nRF52840-Dongle (on Debian/Ubuntu Linux) to monitor 802.15.4 traffic exchanged between Raspi/SAMR21-xpro:

- Nordic nRF52840 Dongle: https://www.nordicsemi.com/Software-and-tools/Development-Kits/nRF52840-Dongle 
- nRF Sniffer for 802.15.4: https://github.com/NordicSemiconductor/nRF-Sniffer-for-802.15.4

We flashed the nRF52840-Dongle as described in this tutorial (on a Debian 10 VM) and configured Wireshark as stated. Important is 
to have python3 as standard interpreter, otherwise the python script throws an error after starting Wireshark. 
Also make sure to set the correct channel ID for sniffing.

### Possible Problems with 6LoWPAN, SAMR21-xpro and Raspberry Pi (with Openlabs 802.15.4 radio)

-  6LoWPAN: RIOT does not receive packets from Linux when short_addr is set #11033 
    - GitHub issue: https://github.com/RIOT-OS/RIOT/issues/11033 
    - By default this is not set (i.e. in RIOT gnrc_networking example!)
- Do not use Raspberry Pi 4 with OpenLabs 802.15.4 radio module:
    - At least with the newest available Kernel (i.e. 5.10) it was **only possible to send** 802.15.4 packets to 
      SAMR21-xpro and the Raspi **did not receive** any packets from SAMR21-xpro. 
    - It is better to use Raspberry Pi 3B(+)
        - There it's working with Raspbian STRETCH (Kernel 4.9 (Downgrade) and also with newest Kernel (5.10))
        - It's also working with Raspbian BUSTER and Kernel 5.10
- Monitoring 802.15.4 traffic: Correct channel ID must be set (RIOT-OS default: `26`)
- Communication SAMR21-xpro<->Raspi: The same channel ID (RIOT-OS default: `26`) and PAN ID need to be set (RIOT-OS default: `0x23`)

## Test CoAP communication between Raspi and SAMR21-xpro

* Install `python3-pip` and `aiocoap` on Raspi:

```shell
sudo apt install python3-pip
pip3 install --upgrade "aiocoap[all]"
```

* Clone `aiocoap` repo and start the included simple CoAP server:

```shell
git clone https://github.com/chrysn/aiocoap.git
cd aiocoap
python3 server.py
```
        
* Use gcoap example on SAMR21-xpro (RIOT OS repo already cloned on PC):
  
```shell
cd ~/RIOT/example/gcoap
make BOARD=samr21-xpro         //build gcoap example
make BOARD=samr21-xpro flash       //flah gcoap to samr21-xpro
make BOARD=samr21-xpro term     //start pyterm
```

* [optional] Reset SAMR21-xpro and change 6LoWPAN settings:
  
```shell
ifconfig 7 set pan_id 0xbeef
ifconfig 7 set channel 13
```
        
* Send a CoAP request to the CoAP server:
  
```shell
coap put fe80::1%7 5683 /send test
```

## Run Matrix Synapse Homeserver as Docker Container with SQLite DB

Source/Based on: https://github.com/matrix-org/synapse/tree/develop/docker#running-synapse

___

Tested on Ubuntu 18.04 (used sqlite DB)

0. Install required Python 3.8:

        sudo apt install python3.8

1. Clone Synapse repo:

        git clone https://github.com/matrix-org/synapse
        cd synapse
        
2. Build Synapse Docker image from source:

        docker build -t matrixdotorg/synapse -f docker/Dockerfile .

3. Generate and adapt configuration files:

        docker run -it --rm \
        --mount type=volume,src=synapse-data,dst=/data\
        -e SYNAPSE_SERVER_NAME=synapse \
        -e SYNAPSE_REPORT_STATS=yes \
        matrixdotorg/synapse:latest generate

   Command generates a `homeserver.yaml` in (typically) `/var/lib/docker/volumes/synapse-data/_data`, customise `homeserver.yaml` to use sqlite DB:
   
       sudo nano /var/lib/docker/volumes/synapse-data/_data/homeserver.yaml
       
    Changed following part to:
    
        # Example Postgres configuration:
        #
        #database:
        #  name: psycopg2
        #  args:
        #    user: synapse
        #    password: Our_supersecret_postgrespassword
        #    database: synapse
        #    host: postgres
        #    cp_min: 5
        #    cp_max: 10
        #
        # For more information on using Synapse with Postgres, see `docs/postgres.md`.
        #
        database:
          name: sqlite3
          args:
            database: /data/homeserver.db
    
    Enable user registration for the Matrix Synapse homeserver:
    
        ## Registration ## 
        # 
        # Registration can be rate-limited using the parameters in the "Ratelimiting" 
        # section of this file.    
                               
        # Enable registration for new users.   
        #   
        enable_registration: true

    Session lifetime can be defined (by default it is infinite):
        
        # Time that a user’s session remains valid for, after they log in.
        #
        # Note that this is not currently compatible with guest logins.
        #
        # Note also that this is calculated at login time: changes are not applied
        # retrospectively to users who have already logged in.
        #
        # By default, this is infinite.
        #
        #session_lifetime: 24h

4. Run Matrix Synapse Docker container:

        docker run -d --name synapse \
        --mount type=volume,src=synapse-data,dst=/data\ 
        -p 8008:8008 \
        matrixdotorg/synapse:latest
        
    Matrix Synapse homeserver is available at http://localhost:8008, but a client (e.g. Element) is necessary to access it.
    
5. If anything goes wrong:

        docker logs synapse    # check the docker logs
        docker container ls    # list running container
        docker container stop synapse  # stop container
        docker container start synapse # start container
        docker container rm synapse # delete container
        docker image rmsynapse # delete synapse docker image and build a new one

___

**Links/additional resources:**

- Synapse installation without Docker is described here: https://github.com/matrix-org/synapse/blob/master/INSTALL.md
- Installed Synapse version can be identified via: http://localhost:8008/_matrix/federation/v1/version
- http://localhost:8008/_matrix/client/versions shows the versions of the Client-Server API supported (and unstable features).

## Run Matrix Synapse Homeserver as Docker Container with Postgres DB

Create an .env file as described in the [.env.example](resources/.env.example) file:

    # Postgres DB connection
    POSTGRES_PASSWORD=secretpassword
    POSTGRES_DB=synapse
    POSTGRES_USER=synapse_user


    # synapse settings
    SYNAPSE_SERVER_NAME=synapse
    SYNAPSE_REPORT_STATS=yes

Download and run a Postgres Docker image (check for correct path of `.env` file):

        docker run --name postgres --env-file=./resources/.env  -p 5432:5432 -d  --mount type=volume,src=postgres-data,dst=/data postgres:11

Connect to container and create the Synapse database:

	docker exec -it postgres psql -U  synapse_user
	CREATE DATABASE synapse OWNER synapse_user ENCODING utf8 LC_COLLATE "C" LC_CTYPE "C" TEMPLATE template0;
	\l 				# show  databases
	\q 				# exit

Generate a new volume and Matrix homeserver configuration file (check for correct path of `.env` file): 

     docker run -it --rm  --env-file=./resources/.env  --mount type=volume,src=synapse-data,dst=/data matrixdotorg/synapse:latest generate

The `homeserver.yaml` is (typically) stored in `/var/lib/docker/volumes/synapse-data/_data`; customise `homeserver.yaml` to use Postgres database:
   
      sudo nano /var/lib/docker/volumes/synapse-data/_data/homeserver.yaml
       
Change the following part to use a Postgres DB:
    
    # Example Postgres configuration:
        
    database:
       name: psycopg2
       args:
         user: synapse_user
         password: secretpassword
         database: synapse
         host: postgres
         cp_min: 5
         cp_max: 10
        

and enable Matrix Synapse registration: uncomment and set to `true`

    # Enable registration for new users.
    enable_registration: true

The current release (Synapse v1.34) has a writing permission issue (cf. https://github.com/matrix-org/synapse/issues/9970), therefore open the `synapse.log.config` and change the log file path:

    sudo nano /var/lib/docker/volumes/synapse-data/_data/synapse.log.config

    filename: /data/homeserver.log
    
Start the Matrix-Synapse Docker container:

    docker run -d --env-file=./resources/.env  --name synapse --mount type=volume,src=synapse-data,dst=/data --link postgres:postgres -p 8008:8008 matrixdotorg/synapse:latest

## User and room creation at Matrix Synapse homeserver

Platform: Debian / Ubuntu (64-bit)

Entities:

* **testuser** 
    * Installs Element (Matrix client)
    * Creates a Matrix room
    * Sends an invite to the SAMR21-xpro users
* **samr21node**
    * Creates an access token via login
    * Joins the room

1. Install Matrix Client "Element-Desktop" for Testing purpose:

   Source: https://element.io/get-started

        sudo apt install -y wget apt-transport-https
        sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg
        echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list
        sudo apt update
        sudo apt install element-desktop

    Alternatively you can run "Element-Web" as Docker container and access it via http://localhost (source: https://github.com/vector-im/element-web):

        docker run -p 80:80 vectorim/riot-web

2. Register the following users (these are created for testing purposes): admin, testuser and samr21node

        docker exec -it synapse register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml -u admin -a -p <adminpwd>
        docker exec -it synapse register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml -u testuser -p <testpwd>
        docker exec -it synapse register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml -u samr21node -p <samr21pwd>
        

3. Start the Element client and login as testuser, create a new room, and check the room ID in the room settings:
![Element Login/Homeserver Config](resources/element_homeserver_config.png "Element Login/Homeserver Config")
![Element check room ID](resources/element_room_id.png "Element check room ID")

4. Create an invite for the  `@samr21node:synapse` user (Element.io client: Room options->Invite people), this creates an invite event which can be viewed by view source:

        {
          "type": "m.room.member",
          "sender": "@testuser:synapse",
          "content": {
            "membership": "invite",
            "displayname": "samr21node"
          },
          "state_key": "@samr21node:synapse",
          "origin_server_ts": 1619515917506,
          "unsigned": {
            "age": 106
          },
          "event_id": "$pwssXPgZ6_qiliLYmG2Kcj0vZPKUo_cWPKdp7zspc0Q",
          "room_id": "!kYsGiNgnRRVZCizHAF:synapse"
        }

Create invite via commandline:

        curl -XPOST -d '{"user_id":"@myfriend:localhost"}' "http://localhost:8008/_matrix/client/r0/rooms/<room_id>/invite?access_token=<YOUR_ACCESS_TOKEN>"

5. [Optional] Get the access token for the `samr21node` user via commandline:

        curl -XPOST -d '{"type":"m.login.password", "user":"samr21node", "password":"samr21pwd"}' "http://localhost:8008/_matrix/client/r0/login"
        {"user_id":"@samr21node:synapse","access_token":"<access token>","home_server":"synapse","device_id":"OPJPRXXURH"}

6. [Optional] Join the room with the `samr21node` user:
    
         curl -XPOST -d '{ "room_id": "!kYsGiNgnRRVZCizHAF:synapse"}' "http://localhost:8008/_matrix/client/r0/rooms/%21kYsGiNgnRRVZCizHAF:synapse/join?access_token=<access token>"

## CoMatrix gateway setup

See [Gateway/README.md](Gateway/README.md) for a detailed howto. 

## CoMatrix client setup

See [Client/README.md](Client/README.md) for a detailed howto.