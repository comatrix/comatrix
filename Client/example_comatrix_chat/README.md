# Example CoMatrix Chat

A prototype application, which provides a simple command line chat client using the CoMatrix module, which translates the Matrix protocol for IoT devices to CoAP. The CoMatrix client is communicating via the CoAP protocol with the gateway, which serves as a CoAP to HTTP proxy and forwards HTTP requests to a Matrix-Synapse homeserver.

## Preliminaries

* **Tested environment**
  * Code tested on a SAMR21-xpro microcontroller with RIOT OS (Release-2021.04,tag 46bc55f514).
  * Tested toolchain: GNU Arm Embedded Toolchain 9-2020-q2-update
* **Setup**
  * Reqirements: Running CoMatrix gateway and Matrix-Synapse homeserver (Follow the steps in the [SETUP.md](../../SETUP.md) tutorial)
  * Copy the Client folder into the `RIOT/example/` folder of your RIOT base directory.

## Client Configuration

The application specific settings must be set in an `app.config file`, consider the `app.config.example`.
* Matrix-Synapse homeserver endpoint (e.g. `CONFIG_COMATRIX_SYNAPSE="http://192.168.1.1:8008"`)
* Gateway link-local IPv6 address (e.g. `CONFIG_COMATRIX_GATEWAYIP6="fe80::1"`)

Optional also the Matrix room ID and user access token can be set hardcoded for devices which lack user input interfaces (keyboard).
* Matrix room ID (e.g. `CONFIG_COMATRIX_ROOMID="!kYsGiNgnRRVZCizHAF:synapse"`)
* Matrix-Synapse homeserver user specific access token (e.g. `CONFIG_COMATRIX_TOKEN="MDAxNWxvY..."`)

* Default 6LoWPAN and WPAN settings:
  * default WPAN channel: `26`
  * default 6LoWPAN PANID: `0x23`

## Compilation

Change to the application root folder (`RIOTBASE/example/Client/example_comatrix_chat`) and build the application via `make` for the SAMR21-xpro board and flash the binary firmware on the microcontroller:

```shell
make BOARD=samr21-xpro
make BOARD=samr21-xpro flash
```

Enable serial communication over an USB cable between SAMR21-xpro and a PC using `pyterm` (`python -m pip install pyterm`) and connect to the SAMR21-xpro:

```
make BOARD=samr21-xpro term
```

## Usage

The `comatrix` shell command provides following Matrix commands: registration, login, logout, joining a Matrix room and receiving and sending messages. It can be invoked as follows:

```
"usage: comatrix <register|login|sendmsg|receivemsg|join|logout>"
```

* **CoMatrix Registration**
  ```
  usage: comatrix register <username> <password>
  ```
  Sends a registration request proxied by the gateway to the Matrix-Synapse homeserver. Registration needs to be enabled in the `homeserver.yaml` settings of the Synapse (, in case of docker deployment: `/var/lib/docker/volumes/synapse-data/_data/homeserver.yaml`). The successful response is a new Synapse access token, which is stored in the CoMatrix client state.

* **CoMatrix login**
  ```
  usage: comatrix login <username> <password>
  ```
  Sends a registration request proxied by the gateway to the Matrix-Synapse homeserver. In case of a successful response a new Synapse access token is set in the CoMatrix client state.

* **CoMatrix sendmsg**
  ```
  usage: comatrix sendmsg <msg>
  ```
  Sends a PUT message request proxied by the gateway to the Matrix-Synapse homeserver into a specific room. <msg> is a text you want to share in the room. Be brief, the size is limited to the command line shell buffer.

* **CoMatrix receivemsg**
  ```
  usage: comatrix receivemsg
  ```
  Sends a GET message request proxied by the gateway to to the Matrix-Synapse homeserver into a specific room. The response contains only the last message of the room (in format sender:text).

* **CoMatrix join**
  ```
  usage: comatrix join <room_id>
  ```
  Sends a POST join request proxied by the gateway to to the Matrix-Synapse homeserver for a specific room. Use the Element.io Matrix client to find out the room ID (`Room->Settings->Advanced`). The room ID starts with an ! and has a : in between the host domain. (example room ID: `"!kYsGiNgnRRVZCizHAF:synapse"`)

* **CoMatrix logout**
  ```
  usage: comatrix logout
  ```
  Sends a POST logout request proxied by the gateway to to the Matrix-Synapse homeserver. On success this invalidates the access token in the CoMatrix client state.


## Future Work

* Enable encryption of the communication to the CoMatrix gateway either via DTLS support, OSCORE or OpenThread.
