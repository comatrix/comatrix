/*
 *
 * CoMatrix is free software and this file is published under the GPLv3 license as
 * described in the accompanying LICENSE file.
 * This Software is for educational purposes only, do not use in production environments.
 *
 * @ingroup     example comatrix chat
 * @{
 *
 * @file        main.c
 * @brief       comatrix chat client application - enables Matrix chat for IoT devices
 *
 * @author      ines - ines.kramer@fh-campuswien.ac.at
 * @author      tobi - tobias.buchberger@fh-campuswien.ac.at
 *
 * @}
 */

#include <stdio.h>
#include "msg.h"
#include "net/gcoap.h"
#include "shell.h"
#include "../comatrix/include/comatrix.h"


#define BUFLEN             (64U)
#define MAIN_QUEUE_SIZE    (4)

static msg_t _main_msg_queue[MAIN_QUEUE_SIZE];
static char  line_buf[SHELL_DEFAULT_BUFSIZE];

/* Internal functions */
static void _comatrixrecmsg_callback(void *data);
static void _comatrixgen_callback(void *data);



/**
 *  @brief interprets command line arguments and calls
 *
 *  @param [in] argc number of command line arguments
 *  @param [in] argv array of command line arguments
 *
 *  @return 0 when comatrix request of command is sent or -1 if anything failed
 */
int comatrix_cli(int argc, char **argv) {
   char buf[BUFLEN] = { 0 };

   if (argc < 2) {
      puts("usage: comatrix <login|register|sendmsg|receivemsg|join|logout>");
      return(1);
   }

   if (strcmp(argv[1], "sendmsg") == 0) {
      //read in txt SHELL_DEFAULT_BUFSIZE is 128 byte
      size_t len = snprintf(buf, BUFLEN, "%s", argv[2]);
      for (int i = 3; i < argc; ++i) {
         len += snprintf(buf + len, BUFLEN - len, " %s", argv[i]);
      }
      comatrix_sendmsg(buf, len,0);
      return(0);
   }
   else if (strcmp(argv[1], "register") == 0) {
      if (argc != 4) {
         puts("usage: comatrix register <username> <password>");
         return(1);
      }
      else{
         int err = comatrix_register(argv[2], argv[3],_comatrixgen_callback);
         if (err > 0) {
            puts("Register request sent.\n");
            return(0);
         }
         else{
            puts("Sending register request failed.\n");
            return(1);
         }
      }
   }
   else if (strcmp(argv[1], "login") == 0) {
      if (argc != 4) {
         puts("usage: comatrix login <username> <password>");
         return(1);
      }
      else{
         int err = comatrix_login(argv[2], argv[3],_comatrixgen_callback);
         if (err > 0) {
            puts("Login request sent.\n");
            return(0);
         }
         else{
            puts("Sending login request failed.\n");
            return(1);
         }
      }
   }
   else if (strcmp(argv[1], "receivemsg") == 0) {
      int len = comatrix_recmsg(_comatrixrecmsg_callback);
      if (len > 0) {
         puts("Receive message request sent.\n");
      }
      else{
         puts("Sending receive message request failed.\n");
      }

      return(0);
   }
   else if (strcmp(argv[1], "join") == 0) {
      if (argc != 3) {
         puts("usage: comatrix join <room_id>\n");
         puts("example: !kYsGiNgnRRVZCizHAF:synapse ");
         return(1);
      }
      int len = comatrix_join(argv[2],_comatrixgen_callback);
      if (len > 0) {
         printf("Join request sent\n");
      }
      else{
         puts("Sending send join request failed.\n");
      }
      return(0);
   }
   else if (strcmp(argv[1], "logout") == 0) {
      int len = comatrix_logout(_comatrixgen_callback);
      if (len > 0) {
         printf("Logout request sent\n");
      }
      else{
         puts("Sending logout request failed.\n");
      }
      return(0);
   }
   else{
      puts("usage: comatrix <login|register|sendmsg|receivemsg|join|logout>");
   }

   return(1);
}

/* Callback handler for receive message function */
static void _comatrixrecmsg_callback(void *data) {
   comatrix_msg *msg = (comatrix_msg *)data;
   printf("CoAP response code: %d\n", msg->coap_code);
   printf(" \n %s: %s\n", msg->sender, msg->text);
   return;
}
/* Generic callback handler */
static void _comatrixgen_callback(void *data) {
   comatrix_resp *resp = (comatrix_resp *)data;
   printf("CoAP response code: %d\n", resp->coap_code);
   if (resp->comatrix_time!=0){
     printf("Timestamp: %d\n",resp->comatrix_time);
   }
   return;
}

/* Enables comatrix_cli commands */
static const shell_command_t shell_commands[] = {
   { "comatrix", "comatrix chat", comatrix_cli },
   { NULL,       NULL,            NULL         }
};

int main(void) {
   /* Initialize message queue for the thread running the shell */
   msg_init_queue(_main_msg_queue, MAIN_QUEUE_SIZE);
   /* Initialize CoMatrix client state */
   comatrix_init(_comatrixgen_callback);
   shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);

   /* Should never be reached */
   return(0);
}
