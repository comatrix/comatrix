/*
 *
 * CoMatrix is free software and this file is published under the GPLv3 license as
 * described in the accompanying LICENSE file.
 * This Software is for educational purposes only, do not use in production environments.
 *
 * @ingroup     comatrix external module
 * @{
 *
 * @file        comatrix.h
 * @brief       Enables Matrix protocol for IoT devices
 *
 * @author      ines - ines.kramer@fh-campuswien.ac.at
 * @author      tobi - tobias.buchberger@fh-campuswien.ac.at
 *
 * @}
 */

/*
 * CoMatrix provides a high-level interface for Matrix client-server communication
 * (https://matrix.org/docs/spec/client_server/r0.6.1.html) using gcoap CoAP message requests
 * via RIOT's sock networking API. Only basic functionality is implemented, which enables a
 * IoT device to register, login and logout at a Matrix-Synapse homeserver, to join a
 * specific Matrix room where the user is invited to. As well as sending messages into a
 * specified Matrix room and retrieve the last message of a room.
 *
 * ## Requirements
 *
 * * CoMatrix Client
 *    - Hardware: tested on SAMR21-xpro board
 *    - Software: tested on RIOT OS (Release-2021.04,tag 46bc55f514)
 * * CoMatrix Environment
 *    - the usage of this client module requires a deployed and reachable CoMatrix gateway
 *      and Matrix-Synapse homeserver. Follow the steps in the SETUP.md file.
 *
 * ## Dependencies
 *
 *    The application Makefile needs to include following modules:
 *    * gnrc_netdev_default
 *    * auto_init_gnrc_netif
 *    * gnrc_ipv6_router_default
 *    * gcoap
 *    * comatrix (as an external module)
 *
 * ## CoMatrix Configuration
 *
 * Application specific settings must be set in an app.config file, consider the
 * app.config.example in the example folders. Required parameters must be set by the
 * CoMatrix init function and stored in the CoMatrix client state.
 *
 * **CONFIG_KCONFIG_USEMODULE_COMATRIX - Enable configuration of CoMatrix module**
 *    * Default: y
 *    * This parameter enables application specific configuration for the external module CoMatrix.
 *
 * **CONFIG_COMATRIX_MAX_TOKEN_LENGTH - Length of the buffer for the Matrix-Synapse
 * homeserver access token**
 *    * Default: 300 bytes
 *    * Due to a change of the length of the Matrix-Synapse access token from around 280
 * bytes to around 40 bytes in the release Synapse 1.34.0 (2021-05-17) the bandwidth
 * consumption has been reduced. For compatibility reasons with older versions of the Synapse
 * homeserver we still kept the default value at 300 bytes. Reduce this the
 * CONFIG_COMATRIX_MAX_TOKEN_LENGTH value to 50 bytes in the app.config file if you use the
 * new release.
 *
 * **CONFIG_COMATRIX_MAX_ROOMID_LENGTH - Length of the buffer for the Matrix room ID**
 *    * Default: 50 bytes
 *
 * **CONFIG_COMATRIX_TXID_LENGTH - Bit-length of the initialization value of the Matrix message ID**
 *   * Default: 0xffff
 *   * During the initialization of the client state a timestamp is retrieved from the CoMatrix
 * gateway and a part form the timestamp is used as message ID. This value defines the number of the
 * least significant bits which are extracted by a bitwise And operation.
 *
 * **CONFIG_COMATRIX_ENABLE_SHORTURL - Set the proxy url mode to short**
 *    * Default: n
 *    * Constrained environments may also have limited bandwidth demands, therefore the CoMatrix
 * client library provides the usage of short proxy-urls. The CoMatrix gateway  will map to the
 * correct url targeting the Matrix-Synapse homeserver according to the proposal in
 * (https://github.com/matrix-org/matrix-doc/blob/7d20c1d9c19972fa63d1d9c124c3656928c28c29/proposals/
 * 3079-low-bandwidth-csapi.md#appendix-b-coap-path-enums). By now just the  short proxy-url mapping
 * is implemented and not the CBOR integer keys.
 *
 * **CONFIG_COMATRIX_ROOMID - Specific Matrix room ID**
 *    * Default: Required (specially for devices with input options)
 *    * This parameter must be set in the app.config file or the CoMatrix join function needs to be
 * called setting a new Matrix room ID. Use Element.io to find the Matrix room ID specific
 * Room->Settings->Advanced. The Matrix room ID starts with an ! and has a : in between the host
 * domain. Example Matrix room ID: "!base64string:synapse host"
 *
 * **CONFIG_COMATRIX_GATEWAYIP6 - IPv6 address of CoMatrix Gateway**
 *    * Default: "fe80::1"
 *    * If you follow the setup steps, the gateway will be configured to "fe80::1" to keep the
 * address short. Otherwise determine the link local address of the lowpan0 interface on the gateway
 * with ifconfig.
 *
 * **CONFIG_COMATRIX_SYNAPSE - HTTP schema and address or domain and port of the Matrix-Synapse
 * homeserver**
 *    * Default: Required
 *    * Use this parameter to define the HTTP schema and the Synapse endpoint address or domain and
 * port such as "https://192.168.1.102:8008".This parameter must be set in the app.config file
 *
 * **CONFIG_COMATRIX_TOKEN - Optional Matrix-Synapse homeserver access token for an user**
 *    * Default: Required (for devices with no input options)
 *    * This token is sent in authenticated CoAP requests as header option with the identifier 256
 * as proposed in (https://github.com/matrix-org/matrix-doc/blob/7d20c1d9c19972fa63d1d9c124c3656928c28c29/proposals/3079-low-bandwidth-csapi.md#appendix-b-coap-path-enums).
 * For devices without input options (keyboard), the Matrix-Synapse homeserver access token must be
 * set in the application configuration file. Otherwise this user specific access token must be
 * generated and set with the registration or login function, which send an authentication request
 * to the Matrix-Synapse homeserver.
 *
 * **Further settings:**
 *
 * **CONFIG_GCOAP_PDU_BUF_SIZE - CoAP packet buffer size**
 *   * Default: 500
 *   * The length of the UDP packet buffer is changed from the gcoap default value of 128 bytes to
 * 500 bytes. Maybe the default value will change in future (https://github.com/RIOT-OS/RIOT/pull/16377. Change
 * this value according to your application requirements.
 *
 * **CONFIG_GCOAP_NON_TIMEOUT - Timeout for confirmable requests**
 *   * Default: 10000000
 *   The timeout value for confirmable CoAP requests has been increased, because we expect that the
 * HTTP proxying to the Matrix-Synapse homeserver takes more time than the communication with a
 * local proxy server.
 *
 * ## Brief description of functionality

 * CoMatrix Init
 *    - initializes network and CoMatrix settings stored in client state
 *    - sends a CoAP timestamp request to the gateway
 *    - calls the _comatrixlogin_resp_handler response handler, on success a part of the time value
 *      is stored in client state and used as CoMatrix text message counter
 * CoMatrix Register
 *    - expects a valid Matrix username and password
 *    - sends a CoAP registration request with CBOR encoded payload of username and password to
 *      the gateway
 *    - calls the _comatrixlogin_resp_handler response handler which on success sets a Synapse access
 *      token in CoMatrix client state
 * CoMatrix Login
 *    - expects a valid Matrix username and password
 *    - sends a CoAP login request with CBOR encoded payload of username and password to the
 *      gateway
 *    - calls the _comatrixlogin_resp_handler response handler which on success sets a Synapse access
 *      token in CoMatrix client state
 * CoMatrix Logout
 *    - sends a CoAP logout request to the gateway
 *    - calls the _comatrixlogout_resp_handler on success the access token is invalidated at the
 *      Matrix-Synapse homeserver
 * CoMatrix Join
 *    - expects a Matrix room id
 *    - sends a CoAP join message request to the gateway
 *    - calls the _comatrixjoin_resp_handler response handle which on success sets the new
 *      access token in the CoMatrix client state
 * CoMatrix Sendmsg
 *    - expects a message string and sends a CoAP send message request with CBOR encoded msg
 *      payload to the gateway, type is non-confirmable and no response handler is called
 * CoMatrix Recmsg*
 *    - expects a pointer to the response message
 *    - sends a CoAP send message request to the gateway
 *    - calls the _comatrixrecmsg_resp_handler which parses the new CoMatrix message and passes a
 *      pointer to the message to the application callback function
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xtimer.h>
#include "net/gcoap.h"
#include "od.h"

#define COMATRIX_OPT_NUM                     256         /**< CoAP option number for Synapse user access tokens */
#define COMATRIX_LOGIN_PATH                  "/login"    /**< CoMatrix gateway path for the login request */
#define COMATRIX_SENDMSG_PATH                "/send"     /**< CoMatrix gateway path for the send message request */
#define COMATRIX_RECMSG_PATH                 "/getmsg"   /**< CoMatrix gateway path for the receive message request */
#define COMATRIX_TIME_PATH                   "/time"     /**< CoMatrix gateway path for the timestamp request */
#define COMATRIX_JOIN_PATH                   "/join"     /**< CoMatrix gateway path for the join request */
#define COMATRIX_LOGOUT_PATH                 "/logout"   /**< CoMatrix gateway path for the logout request */
#define COMATRIX_REGISTER_PATH               "/register" /**< CoMatrix gateway path for the registration request */
#define CONFIG_COMATRIX_MAX_SENDER_LENGTH    20          /**< CoMatrix message max sender length */
#define CONFIG_COMATRIX_MAX_TEXT_LENGTH      64          /**< CoMatrix message max text length */
#define CONFIG_COMATRIX_TXID_LENGTH          0xffff      /**< Used to XOR with timestamp to initialize Matrix message ID stored in client state*/


//debugging - keep this order, first set enable debug and then include the header
// #define ENABLE_DEBUG             (1)
// #include "debug.h"


/* definition of types */

/**
 * @ brief CoMatrix internal client state
 */
typedef struct comatrix_state {
   sock_udp_ep_t remote;                                             /**< UDP remote socket for gateway communication */
   char          comatrix_token[CONFIG_COMATRIX_MAX_TOKEN_LENGTH];   /**< Matrix-Synapse homeserver access token */
   char          comatrix_roomid[CONFIG_COMATRIX_MAX_ROOMID_LENGTH]; /**< Matrix room ID */
   int           tx_id;                                              /**< Matrix message transaction ID */
} comatrix_state;

/**
 * @brief   CoMatrix response code
 * todo: extend with matrix error codes
 */
typedef struct comatrix_resp {
   unsigned coap_code;                                             /**< CoAP message's code in decimal format ((class * 100) + detail) */
   int      comatrix_time;                                         /**< Timestamp received from gateway */
} comatrix_resp;


/**
 * @brief   CoMatrix message struct
 * todo: extend with matrix error codes
 */
typedef struct comatrix_msg {
   unsigned coap_code;                                           /**< CoAP message's code in decimal format ((class * 100) + detail) */
   char     sender[CONFIG_COMATRIX_MAX_SENDER_LENGTH];           /**< Sender name of Matrix message */
   size_t   sender_len;                                          /**< Length of sender name */
   char     text[CONFIG_COMATRIX_MAX_TEXT_LENGTH];               /**< Text of Matrix message */
   size_t   text_len;                                            /**< Length of text */
} comatrix_msg;

/**
 * @brief  Callback function for applications
 *
 * @param[in]  Pointer to callback handler of application
 */
typedef void (*comatrix_callback_t)(void *data);

/* CoMatrix client API function definitions */

/**
 *  @brief Sends a COAP join POST request to the gateway and calls the _comatrixrecmsg_resp_handler response handler
 *
 *  @param[in] room_id            Matrix room ID of a Matrix-Synapse homeserver
 *  @param[in] callback_handler   Pointer to callback function in the application could be 0
 *
 *  @return length of the payload
 *  @return 0  when the room ID exceeds PDU payload buffer size
 *  @return -1 when sending CoAP login POST request failed
 */
int comatrix_join(char *room_id, comatrix_callback_t callback_handler);

/**
 *  @brief Sends a COAP logout request to the gateway and calls the _comatrixlogout_resp_handler response handler
 *        on success the access token in the CoMatrix state is invalidated
 *
 *  @param[in] callback_handler   Pointer to callback function in the application, can be 0
 *
 *  @return length of the payload
 *  @return -1 when sending CoAP login POST request failed
 */
int comatrix_logout(comatrix_callback_t callback_handler);

/**
 *  @brief Sends a COAP send message PUT request to the gateway, type is non-confirmable and no response handler is called
 *
 *  Send message request is non-confirmable for constrained devices like sensor nodes, when communication errors occur the packet will not be resent
 *
 *  @param[in] msgbuf               message buffer.
 *  @param[in] msglen               length of message buffer.
 *  @param[in] callback_handler     should be 0 - not implemented yet because it is non-confirmable
 *
 *  @return length of the payload
 *  @return 0   when the message exceeds PDU payload buffer size
 *  @return -1  when CoAP send message PUT request failed
 */
int comatrix_sendmsg(char *msgbuf, size_t msglen, comatrix_callback_t callback_handler);

/**
 *  @brief Sends a COAP receive message request to the gateway and calls the _comatrixrecmsg_resp_handler response handler
 *
 *  @param[in] callback_handler   Callback when message received, should not be NULL
 *
 *  @return length of the payload if the receive msg COAP GET request is sent
 *  @return -1 sending receive msg COAP GET request failed
 */
int comatrix_recmsg(comatrix_callback_t callback_handler);

/**
 *  @brief Sends a COAP login POST request to the gateway
 *  and calls the _comatrixlogin_resp_handler response handler which sets a Synapse access token on success
 *
 *  @param[in] username           matrix username
 *  @param[in] password           matrix password
 *  @param[in] callback_handler   pointer to callback function in the application, can be 0
 *
 *  @return length of the payload
 *  @return 0  when the message exceeds PDU payload buffer size
 *  @return -1 when CoAP login POST request failed
 */
int comatrix_login(char *username, char *password, comatrix_callback_t callback_handler);

/**
 *  @brief Sends a COAP register POST request to the gateway and
 *  calls the _comatrixlogin_resp_handler response handler which sets a Synapse access token on success
 *
 *  @param[in] username           matrix username
 *  @param[in] password           matrix password
 *  @param[in] callback_handler   pointer to callback function in the application, can be 0
 *
 *  @return length of the payload
 *  @return 0  when the message exceeds the PDU payload buffer size
 *  @return -1 when CoAP login POST request failed
 */
int comatrix_register(char *username, char *password, comatrix_callback_t callback_handler);

/**
 *  @brief Initializes client state and sends a COAP GET timestamp request to the Gateway
 *  and calls _comatrixinit_resp_handler response handler
 *
 *  @param[in] callback_handler   pointer to callback function in the application, can be 0
 *
 *  @return 1 if the time COAP GET request is sent, else
 *  @return 0 when link-local address and no valid interface is found
 *  @return -1 on error
 */
int comatrix_init(comatrix_callback_t callback_handler);
