/*
 *
 * CoMatrix is free software and this file is published under the GPLv3 license as
 * described in the accompanying LICENSE file.
 * This Software is for educational purposes only, do not use in production environments.
 *
 * @ingroup     comatrix external module
 * @{
 *
 * @file        comatrix_client.c
 * @brief       comatrix client - enables Matrix protocol for IoT devices
 *
 * @author      ines - ines.kramer@fh-campuswien.ac.at
 * @author      tobi - tobias.buchberger@fh-campuswien.ac.at
 *
 * @}
 **/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xtimer.h>
#include "net/gcoap.h"
#include "random.h"
#include "od.h"
#include "include/comatrix.h"

//debugging - keep this order, first set enable debug and then include the header
#define ENABLE_DEBUG    (1)
#include "debug.h"

/* definition internal functions */
extern int encbor_comatrixmsg(uint8_t *retbuf, char *msgbuf, size_t len);
extern int encbor_comatrixlogin(uint8_t *retbuf, char *username, char *password, size_t cborlen);
extern int decbor_comatrixtoken(comatrix_state *_cstate, uint8_t *payload, size_t payload_len);
extern int decbor_comatrixmsg(comatrix_msg *msg, uint8_t *payload, size_t payload_len);
extern int encbor_comatrixregister(uint8_t *retbuf, char *username, char *password, size_t cborlen);
extern int decbor_comatrixroomid(comatrix_state *_cstate, uint8_t *payload, size_t payload_len);

static void _comatrixlogin_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote);
static void _comatrixrecmsg_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote);
static void _comatrixinit_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote);
static void _comatrixjoin_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote);
static void _comatrixlogout_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote);


/* global variables*/
static comatrix_state _cstate;
static char           separator = '\0';


/**
 *  @brief sends a COAP send message PUT request to the gateway, type is non-confirmable and no response handler is called
 *
 *  Send message request is non-confirmable for very constraint devices like sensor nodes, when communication errors occur the packet will not be resent
 *
 *  @param[in] msgbuf               message buffer.
 *  @param[in] msglen               length of message buffer.
 *  @param[in] callback_handler     should be 0; not implemented yet because it is non-confirmable
 *
 *  @return length of the payload
 *  @return 0   when the message exceeds PDU payload buffer size
 *  @return -1  when CoAP send message PUT request failed
 **/
int comatrix_sendmsg(char *msgbuf, size_t msglen, comatrix_callback_t callback_handler) {
   (void)callback_handler;   /* not used by now because CoAP type is non-confirmable */
   coap_pkt_t pdu;
   uint8_t    pdu_buf[CONFIG_GCOAP_PDU_BUF_SIZE];
   size_t     len       = 0;
   char       separator = '\0';

   /* build proxy string */
  #ifdef CONFIG_COMATRIX_ENABLE_SHORTURL
   int  proxy_buf_size = 20 + strlen(CONFIG_COMATRIX_SYNAPSE) + strlen(_cstate.comatrix_roomid) + snprintf(NULL, 0, "%d", _cstate.tx_id);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/9/%s/m.room.message/%d", CONFIG_COMATRIX_SYNAPSE, _cstate.comatrix_roomid, _cstate.tx_id);
  #else
   int  proxy_buf_size = 49 + strlen(CONFIG_COMATRIX_SYNAPSE) + strlen(_cstate.comatrix_roomid) + snprintf(NULL, 0, "%d", _cstate.tx_id);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/_matrix/client/r0/rooms/%s/send/m.room.message/%d", CONFIG_COMATRIX_SYNAPSE, _cstate.comatrix_roomid, _cstate.tx_id);
  #endif
   DEBUG("[comatrix_sendmsg:] proxystring len: %d \n", le);

   /* increment message counter */
   _cstate.tx_id = _cstate.tx_id + 1;

   /* initialize coap packet and set header and options */
   gcoap_req_init(&pdu, pdu_buf, CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_PUT, COMATRIX_SENDMSG_PATH);
   coap_hdr_set_type(pdu.hdr, COAP_TYPE_NON);
   coap_opt_add_format(&pdu, COAP_FORMAT_CBOR);
   coap_opt_add_proxy_uri(&pdu, proxy_buf);
   coap_opt_add_chars(&pdu, COMATRIX_OPT_NUM, _cstate.comatrix_token, strlen(_cstate.comatrix_token), separator);
   len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);

   /* validate input length and encode payload to cbor {"msgtype":"m.text","body":""} - 22 bytes cbor without msg */
   if (pdu.payload_len >= msglen + 23) {
      size_t cborlen = encbor_comatrixmsg((uint8_t *)pdu.payload, msgbuf, msglen);;
      len += cborlen;
   }
   else{
      DEBUG("[comatrix_sendmsg:] ERROR. The message buffer is too small for the message\n");
      return(0);
   }
   DEBUG("[comatrix_sendmsg:] sending msg ID %u, %u bytes\n", coap_get_id(&pdu), (unsigned)len);
   if (!gcoap_req_send(pdu_buf, len, &_cstate.remote, NULL, NULL)) {
      DEBUG("[comatrix_sendmsg:] msg send failed\n");
      return(-1);
   }
   return(len);
}

/**
 *  @brief sends a COAP login POST request to the gateway and calls the _comatrixlogin_resp_handler response handler which sets a Synapse access token on success
 *
 *  @param[in] username           matrix username
 *  @param[in] password           matrix password
 *  @param[in] callback_handler   can be 0, pointer to callback function in the application
 *
 *  @return length of the payload
 *  @return 0  when the message exceeds PDU payload buffer size
 *  @return -1 when CoAP login POST request failed
 **/
int comatrix_login(char *username, char *password, comatrix_callback_t callback_handler) {
   coap_pkt_t pdu;
   uint8_t    pdu_buf[CONFIG_GCOAP_PDU_BUF_SIZE];
   size_t     len = 0;

   /* build proxy string */
  #ifdef CONFIG_COMATRIX_ENABLE_SHORTURL
   int  proxy_buf_size = 3 + strlen(CONFIG_COMATRIX_SYNAPSE);
   char proxy_buf[proxy_buf_size];
   int  le = (int)snprintf(proxy_buf, proxy_buf_size, "%s/1", CONFIG_COMATRIX_SYNAPSE);
  #else
   int  proxy_buf_size = 25 + strlen(CONFIG_COMATRIX_SYNAPSE);
   char proxy_buf[proxy_buf_size];
   int  le = (int)snprintf(proxy_buf, proxy_buf_size, "%s/_matrix/client/r0/login", CONFIG_COMATRIX_SYNAPSE);
  #endif

   DEBUG("[comatrix login:] proxystring len: %d \n", le);

   /* initialize coap packet and set header and options */
   gcoap_req_init(&pdu, pdu_buf, CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_POST, COMATRIX_LOGIN_PATH);
   coap_hdr_set_type(pdu.hdr, COAP_TYPE_CON);
   coap_opt_add_format(&pdu, COAP_FORMAT_CBOR);
   coap_opt_add_proxy_uri(&pdu, proxy_buf);
   len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);

   /* validate input length and encode payload to cbor {"type":"m.login.password", "identifier": {"type":"m.id.user", "user":""}, "password":""} -> 66 bytes cbor */
   size_t cbor_len = strlen(username) + strlen(password) + 68;
   if (pdu.payload_len >= cbor_len) {
      size_t cborlen = encbor_comatrixlogin((uint8_t *)pdu.payload, username, password, cbor_len);
      len += cborlen;
   }
   else{
      DEBUG("[comatrix_login:] ERROR. The message buffer is too small for the login message\n");
      return(0);
   }


   DEBUG("[comatrix_login:] sending msg ID %u, %u bytes\n", coap_get_id(&pdu), (unsigned)len);
   if (!gcoap_req_send(pdu_buf, len, &_cstate.remote, _comatrixlogin_resp_handler, callback_handler)) {
      DEBUG("[comatrix_login:] send login request failed\n");
      return(-1);
   }
   return(len);
}

/**
 *  @brief sends a COAP register POST request to the gateway and calls the _comatrixlogin_resp_handler response handler which sets a Synapse access token on success
 *  @param [in] username --  matrix username
 *  @param [in] password -- matrix password
 *
 *  @return length of the payload
 *  @return 0  when the message exceeds PDU payload buffer size
 *  @return -1 when CoAP login POST request failed
 **/
int comatrix_register(char *username, char *password, comatrix_callback_t callback_handler) {
   coap_pkt_t pdu;
   uint8_t    pdu_buf[CONFIG_GCOAP_PDU_BUF_SIZE];
   size_t     len = 0;

   /* build proxy string */
 #ifdef CONFIG_COMATRIX_ENABLE_SHORTURL
   int  proxy_buf_size = 3 + strlen(CONFIG_COMATRIX_SYNAPSE);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/4", CONFIG_COMATRIX_SYNAPSE);
 #else
   int  proxy_buf_size = 28 + strlen(CONFIG_COMATRIX_SYNAPSE);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/_matrix/client/r0/register", CONFIG_COMATRIX_SYNAPSE);
 #endif

   DEBUG("[comatrix login:] proxystring len: %d \n", le);

   /* initialize coap packet and set header and options */
   gcoap_req_init(&pdu, &pdu_buf[0], CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_POST, COMATRIX_REGISTER_PATH);
   coap_hdr_set_type(pdu.hdr, COAP_TYPE_CON);
   coap_opt_add_format(&pdu, COAP_FORMAT_CBOR);
   coap_opt_add_proxy_uri(&pdu, proxy_buf);
   len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);


   size_t cbor_len = strlen(username) + strlen(password) + 48;

   /* validate input length and encode payload to cbor {"username":"", "password":"", "auth": {"type":"m.login.dummy"}}-> 46 bytes + 2 bytes for variable length */
   if (pdu.payload_len >= cbor_len) {
      size_t cborlen = encbor_comatrixregister((uint8_t *)pdu.payload, username, password, cbor_len);
      len += cborlen;
   }
   else{
      DEBUG("[comatrix_register:] ERROR. The message buffer is too small for the message\n");
      return(0);
   }

   DEBUG("[comatrix_register:] sending msg ID %u, %u bytes\n", coap_get_id(&pdu), (unsigned)len);
   if (!gcoap_req_send(&pdu_buf[0], len, &_cstate.remote, _comatrixlogin_resp_handler, callback_handler)) {
      DEBUG("[comatrix_register:] send logout request failed\n");
      return(-1);
   }
   return(len);
}

/* response handler for comatrix_login and comatrix_register */
static void _comatrixlogin_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote) {
   (void)remote;
   int len = 0;
   if (memo->state == GCOAP_MEMO_TIMEOUT) {
      DEBUG("gcoap: timeout for msg ID %02u\n", coap_get_id(pdu));
      return;
   }
   else if (memo->state == GCOAP_MEMO_ERR) {
      DEBUG("gcoap: error in response\n");
      return;
   }
   /* decode cbor response payload and set token */
   if ((pdu->payload_len > 0) & (coap_get_code_class(pdu) == COAP_CLASS_SUCCESS) & (coap_get_content_type(pdu) == COAP_FORMAT_CBOR)) {
      len = decbor_comatrixtoken(&_cstate, pdu->payload, (size_t)pdu->payload_len);
   }
   /* if there is a callback function set response code */
   if (memo->context != NULL) {
      comatrix_resp resp;
      resp.coap_code = coap_get_code(pdu);
      /* todo: set here matrix error code */
      comatrix_callback_t call = memo->context;
      call(&resp);
   }
   else{
      if ((len > 0) & (coap_get_code_class(pdu) == COAP_CLASS_SUCCESS)) {
         DEBUG("[_comatrixlogin_resp_handler:] new comatrix access-token set.\n");
      }
      else{
         DEBUG("[_comatrixlogin_resp_handler:] Failed to set new token.\n");
      }
   }
   return;
}

/**
 *  @brief sends a COAP receive message request to the gateway calls the _comatrixrecmsg_resp_handler response handler
 *
 *  @param[in] callback_handler   Callback when message received, should not be NULL
 *
 *  @return length of the payload if the receive msg COAP GET request is sent
 *  @return -1 on error
 */
int comatrix_recmsg(comatrix_callback_t callback_handler) {
   coap_pkt_t pdu;
   uint8_t    pdu_buf[CONFIG_GCOAP_PDU_BUF_SIZE];
   size_t     len = 0;

   /* build proxy string */
 #ifdef CONFIG_COMATRIX_ENABLE_SHORTURL
   int  proxy_buf_size = 4 + strlen(CONFIG_COMATRIX_SYNAPSE) + strlen(_cstate.comatrix_roomid);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/9/%s", CONFIG_COMATRIX_SYNAPSE, _cstate.comatrix_roomid);
 #else
   int  proxy_buf_size = 50 + strlen(CONFIG_COMATRIX_SYNAPSE) + strlen(_cstate.comatrix_roomid);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/_matrix/client/r0/rooms/%s/messages?dir=b&limit=1", CONFIG_COMATRIX_SYNAPSE, _cstate.comatrix_roomid);
 #endif

   DEBUG("[comatrix_recmsg:] proxystring len: %d \n", le);

   /* initialize coap packet and set header and options */
   gcoap_req_init(&pdu, &pdu_buf[0], CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_GET, COMATRIX_RECMSG_PATH);
   coap_hdr_set_type(pdu.hdr, COAP_TYPE_CON);
   coap_opt_add_proxy_uri(&pdu, proxy_buf);
   coap_opt_add_chars(&pdu, COMATRIX_OPT_NUM, _cstate.comatrix_token, strlen(_cstate.comatrix_token), separator);
   len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);

   DEBUG("[comatrix_recmsg:] sending msg ID %u, %u bytes\n", coap_get_id(&pdu), (unsigned)len);
   if (!gcoap_req_send(pdu_buf, len, &_cstate.remote, _comatrixrecmsg_resp_handler, callback_handler)) {
      DEBUG("[comatrix recmsg:] send receive msg request failed\n");
      return(-1);
   }
   return(len);
}

/* response handler for comatrix_recmsg */
static void  _comatrixrecmsg_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote) {
   (void)remote;
   if (memo->state == GCOAP_MEMO_TIMEOUT) {
      DEBUG("gcoap: timeout for msg ID %02u\n", coap_get_id(pdu));
      return;
   }
   else if (memo->state == GCOAP_MEMO_ERR) {
      DEBUG("gcoap: error in response.");
      return;
   }
   if (memo->context != NULL) {
      comatrix_msg newmsg;
      if ((pdu->payload_len > 0) & (coap_get_code_class(pdu) == COAP_CLASS_SUCCESS) & (coap_get_content_type(pdu) == COAP_FORMAT_CBOR)) {
         decbor_comatrixmsg(&newmsg, pdu->payload, (size_t)pdu->payload_len);
      }
      newmsg.coap_code = coap_get_code(pdu);
      comatrix_callback_t call = memo->context;
      call(&newmsg);
   }
   else {
      DEBUG("\n[_comatrixrecmsg_resp_handler:], no callback handler implemented code: %u.\n", coap_get_code(pdu));
   }
   return;
}

/**
 *  @brief sends a COAP join request to the gateway and calls the _comatrixrecmsg_resp_handler response handler
 *
 *  @param[in] room_id            matrix room id on synapse server
 *  @param[in] callback_handler
 *
 *  @return length of the payload if the receive msg COAP GET request is sent else -1
 */
int comatrix_join(char *room_id, comatrix_callback_t callback_handler) {
   coap_pkt_t pdu;
   uint8_t    pdu_buf[CONFIG_GCOAP_PDU_BUF_SIZE];
   size_t     len = 0;

   /* validate input */
   if ((strlen(room_id) > CONFIG_COMATRIX_MAX_ROOMID_LENGTH) || ((uint8_t)room_id[0] != 0x21)) {
      DEBUG("[comatrix_join:] roomid too long for buffer.");
      return(0);
   }

   /* build proxy string */
 #ifdef CONFIG_COMATRIX_ENABLE_SHORTURL
   int  proxy_buf_size = 3 + strlen(CONFIG_COMATRIX_SYNAPSE) + strlen(room_id);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/K/%s", CONFIG_COMATRIX_SYNAPSE, room_id);
 #else
   int  proxy_buf_size = 32 + strlen(CONFIG_COMATRIX_SYNAPSE) + strlen(room_id);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/_matrix/client/r0/rooms/%s/join", CONFIG_COMATRIX_SYNAPSE, room_id);
 #endif

   DEBUG("[comatrix_join:] proxystring len: %d \n", le);
   /* initialize coap packet and set header and options */
   gcoap_req_init(&pdu, &pdu_buf[0], CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_POST, COMATRIX_JOIN_PATH);
   coap_hdr_set_type(pdu.hdr, COAP_TYPE_CON);
   coap_opt_add_proxy_uri(&pdu, proxy_buf);
   coap_opt_add_chars(&pdu, COMATRIX_OPT_NUM, _cstate.comatrix_token, strlen(_cstate.comatrix_token), separator);
   len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);

   DEBUG("[comatrix_join:] sending msg ID %u, %u bytes\n", coap_get_id(&pdu), (unsigned)len);
   if (!gcoap_req_send(&pdu_buf[0], len, &_cstate.remote, _comatrixjoin_resp_handler, callback_handler)) {
      DEBUG("[comatrix_join:] send join request failed\n");
      return(-1);
   }
   return(len);
}

/* response handler for comatrix_join - expects cbor encoded room_id which is set in comatrix state*/
static void  _comatrixjoin_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote) {
   (void)remote;
   int len = 0;
   if (memo->state == GCOAP_MEMO_TIMEOUT) {
      DEBUG("gcoap: timeout for msg ID %x\n", (uint8_t)coap_get_id(pdu));
      return;
   }
   else if (memo->state == GCOAP_MEMO_ERR) {
      DEBUG("gcoap: error in response\n");
      return;
   }
   /* decode cbor payload and set room_id in client state */
   if ((pdu->payload_len > 0) & (coap_get_code_class(pdu) == COAP_CLASS_SUCCESS) & (coap_get_content_type(pdu) == COAP_FORMAT_CBOR)) {
      len = decbor_comatrixroomid(&_cstate, pdu->payload, (size_t)pdu->payload_len);
   }
   /* if there is a callback function set response code */
   if (memo->context != NULL) {
      comatrix_resp resp;
      resp.coap_code = coap_get_code(pdu);
      comatrix_callback_t call = memo->context;
      call(&resp);
   }
   else{
      if ((len > 0) & (coap_get_code_class(pdu) == COAP_CLASS_SUCCESS)) {
         DEBUG("[_comatrixjoin_resp_handler:] successfully joined new roomid set.\n");
      }
      else {
         DEBUG("[_comatrixjoin_resp_handler:] failed errorcode %u\n", coap_get_code_raw(pdu));
      }
   }
   return;
}

/**
 *  @brief sends a COAP logout request to the gateway and calls the _comatrixlogout_resp_handler response handler
 *        on success the access token in comatrix state is invalidated
 *
 *  @return len -- of the payload if the logout COAP POST request is sent else -1
 */
int comatrix_logout(comatrix_callback_t callback_handler) {
   coap_pkt_t pdu;
   uint8_t    pdu_buf[CONFIG_GCOAP_PDU_BUF_SIZE];
   size_t     len = 0;

   /* build proxy string */
 #ifdef CONFIG_COMATRIX_ENABLE_SHORTURL
   int  proxy_buf_size = 3 + strlen(CONFIG_COMATRIX_SYNAPSE);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/3", CONFIG_COMATRIX_SYNAPSE);
 #else
   int  proxy_buf_size = 26 + strlen(CONFIG_COMATRIX_SYNAPSE);
   char proxy_buf[proxy_buf_size];
   int  le = snprintf(proxy_buf, proxy_buf_size, "%s/_matrix/client/r0/logout", CONFIG_COMATRIX_SYNAPSE);
 #endif

   DEBUG("[comatrix_logout:] proxystring len: %d \n", le);

   /* initialize coap packet and set header and options */
   gcoap_req_init(&pdu, &pdu_buf[0], CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_POST, COMATRIX_LOGOUT_PATH);
   coap_hdr_set_type(pdu.hdr, COAP_TYPE_CON);
   coap_opt_add_proxy_uri(&pdu, proxy_buf);
   coap_opt_add_chars(&pdu, COMATRIX_OPT_NUM, _cstate.comatrix_token, strlen(_cstate.comatrix_token), separator);
   len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);

   DEBUG("[comatrix_logout:] sending msg ID %u, %u bytes\n", coap_get_id(&pdu), (unsigned)len);

   if (!gcoap_req_send(&pdu_buf[0], len, &_cstate.remote, _comatrixlogout_resp_handler, callback_handler)) {
      DEBUG("[comatrix_logout:] send logout request failed\n");
      return(-1);
   }
   return(len);
}

/* response handler for comatrix_logout */
static void  _comatrixlogout_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote) {
   (void)remote;
   if (memo->state == GCOAP_MEMO_TIMEOUT) {
      DEBUG("gcoap: timeout for msg ID %x\n", (uint8_t)coap_get_id(pdu));
      return;
   }
   else if (memo->state == GCOAP_MEMO_ERR) {
      DEBUG("gcoap: error in response\n");
      return;
   }
   /* if there is a callback function set response code */
   if (memo->context != NULL) {
      comatrix_resp resp;
      resp.coap_code = coap_get_code(pdu);
      comatrix_callback_t call = memo->context;
      call(&resp);
   }
   else{
      if (coap_get_code_class(pdu) == COAP_CLASS_SUCCESS) {
         DEBUG("[_comatrixlogout_resp_handler:] logged out, token invalidated.\n");
      }
      else{
         DEBUG("[_comatrixlogout_resp_handler:] log out failed\n");
      }
   }
   return;
}

/**
 *  @brief initializes comatrix client state
 *
 *  @return 1 -- if the time COAP GET request is sent else -1
 */
int comatrix_init(comatrix_callback_t callback_handler) {
   ipv6_addr_t addr;
   coap_pkt_t  pdu;
   uint8_t     pdu_buf[CONFIG_GCOAP_PDU_BUF_SIZE];
   size_t      len = 0;

   /* validate token length and set access token in comatrix state */
   #ifdef CONFIG_COMATRIX_TOKEN
   if (strlen(CONFIG_COMATRIX_TOKEN) < CONFIG_COMATRIX_MAX_TOKEN_LENGTH) {
      snprintf(_cstate.comatrix_token, CONFIG_COMATRIX_MAX_TOKEN_LENGTH, "%s", CONFIG_COMATRIX_TOKEN);
   }
   else{
      DEBUG("[comatrix_init:] Token is too long for buffer.\n");
   }
   #endif

   /* validate room id length and set room_id in comatrix state */
   #ifdef CONFIG_COMATRIX_ROOMID
   if (strlen(CONFIG_COMATRIX_ROOMID) < CONFIG_COMATRIX_MAX_ROOMID_LENGTH) {
      snprintf(_cstate.comatrix_roomid, CONFIG_COMATRIX_MAX_ROOMID_LENGTH, "%s", CONFIG_COMATRIX_ROOMID);
   }
   else{
      DEBUG("[comatrix_init:] RoomID is too long for buffer.\n");
   }
   #endif

   /* initialize remote udp socket */
   _cstate.remote.family = AF_INET6;

   /* set interface */
   if (gnrc_netif_numof() == 1) {
      /* assign the single interface found in gnrc_netif_numof() */
      _cstate.remote.netif = (uint16_t)gnrc_netif_iter(NULL)->pid;
   }
   else{
      _cstate.remote.netif = SOCK_ADDR_ANY_NETIF;
   }

   /* parse destination address */
   if (ipv6_addr_from_str(&addr, CONFIG_COMATRIX_GATEWAYIP6) == NULL) {
      DEBUG("[comatrix_init:]  unable to parse destination address");
      return(0);
   }

   if ((_cstate.remote.netif == SOCK_ADDR_ANY_NETIF) && ipv6_addr_is_link_local(&addr)) {
      DEBUG("[comatrix_init:]  must specify interface for link local target");
      return(0);
   }
   memcpy(&_cstate.remote.addr.ipv6[0], &addr.u8[0], sizeof(addr.u8));

   /* parse port */
   _cstate.remote.port = CONFIG_GCOAP_PORT;

   /* coap request for timestamp from gateway to set the tx_id value, calls comatrix_init response handler */
   gcoap_req_init(&pdu, &pdu_buf[0], CONFIG_GCOAP_PDU_BUF_SIZE, COAP_METHOD_GET, COMATRIX_TIME_PATH);
   coap_hdr_set_type(pdu.hdr, COAP_TYPE_CON);
   len = coap_opt_finish(&pdu, COAP_OPT_FINISH_PAYLOAD);
   if (!gcoap_req_send(pdu_buf, len, &_cstate.remote, _comatrixinit_resp_handler, callback_handler)) {
      return(-1);
   }
   return(1);
}

/* response handler for comatrix_init */
static void  _comatrixinit_resp_handler(const gcoap_request_memo_t *memo, coap_pkt_t *pdu, const sock_udp_ep_t *remote) {
   (void)remote;
   comatrix_resp resp;
   if (memo->state == GCOAP_MEMO_TIMEOUT) {
      DEBUG("[_comatrixinit_resp_handler:] timeout for msg ID %02u\n Is your gateway online?", coap_get_id(pdu));
      return;
   }
   else if (memo->state == GCOAP_MEMO_ERR) {
      DEBUG("gcoap: error in response\n");
      return;
   }
   if ((pdu->payload_len > 0) & (pdu->payload_len < 12) & (coap_get_content_type(pdu) == COAP_FORMAT_TEXT) & (coap_get_code_class(pdu) == COAP_CLASS_SUCCESS)) {
      int tx_id = atoi((char *)pdu->payload);
      /* set timestamp in response */
      if (memo->context != NULL) {
         resp.comatrix_time = tx_id;
      }
      /* initialize tx_id with n least significant bytes set in CONFIG_COMATRIX_TXID_LENGTH */
      _cstate.tx_id = tx_id & CONFIG_COMATRIX_TXID_LENGTH;
      /* if no callback leave */
      if (memo->context == NULL) {
         DEBUG("[comatrix_init:] successfully initialized.\n");
         return;
      }
   }
   /* if callback set the response code */
   if (memo->context != NULL) {
      resp.coap_code=coap_get_code(pdu);
      comatrix_callback_t call = memo->context;
      call(&resp);
      return;
   }
   else{
      DEBUG("[comatrix_init:] failed to initialize\n");
      return;
   }
}
