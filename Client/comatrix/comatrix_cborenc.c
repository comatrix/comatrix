/*
 *
 * CoMatrix is free software and this file is published under the GPLv3 license as
 * described in the accompanying LICENSE file.
 * This Software is for educational purposes only, do not use in production environments.
 *
 * @ingroup     comatrix external module
 * @{
 *
 * @file        comatrix_cborenc.c
 * @brief       comatrix client - enables Matrix protocol for IoT devices
 *
 * @author      ines - ines.kramer@fh-campuswien.ac.at
 * @author      tobi - tobias.buchberger@fh-campuswien.ac.at
 *
 * @}
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "cbor.h"
#include "include/comatrix.h"

#define ENABLE_DEBUG    (1)
#include "debug.h"


/**
 *  @brief encode payload of comatrix_sendmsg request
 *  JSON example msg: {"msgtype":"m.text","body":"Temp: 23.4"}, len=21+2 bytes msg encoding
 *
 *  @param [out] retbuf     Payload  buffer for encoded cbor
 *  @param [in] msgbuf      Message buffer
 *  @param [in] msglen      Length of message
 *
 *  @return length of cbor encoded message
 * @ return -1 on CBOR encoding error
 */
int encbor_comatrixmsg(uint8_t *retbuf, char *msgbuf, size_t msglen) {
   CborEncoder encoder, map_encoder;

   cbor_encoder_init(&encoder, retbuf, msglen + 23, 0);
   cbor_encoder_create_map(&encoder, &map_encoder, 2);
   cbor_encode_text_stringz(&map_encoder, "msgtype");
   cbor_encode_text_stringz(&map_encoder, "m.text");
   cbor_encode_text_stringz(&map_encoder, "body");
   CborError err = cbor_encode_text_string(&map_encoder, msgbuf, msglen);
   err = cbor_encoder_close_container(&encoder, &map_encoder);
   if (err) {
      DEBUG("[CBOR] encoding error occurred");
      return(-1);
   }
   else{
      return(cbor_encoder_get_buffer_size(&encoder, retbuf));
   }
}

/**
 *  @brief encode payload of comatrix_login request
 *  JSON example credentials: {"type":"m.login.password", "identifier": {"type":"m.id.user", "user":"example"}, "password":"example"}
 *
 *  @param [out] retbuf       Payload buffer for encoded cbor
 *  @param [in] username      Matrix username
 *  @param [in] password      Matrix password
 *  @param [in] cborlen       Length of buffer
 *
 *  @return length of CBOR encoded payload
 *  @return -1 on CBOR encoding error
 */
int encbor_comatrixlogin(uint8_t *retbuf, char *username, char *password, size_t cborlen) {
   CborEncoder encoder, map_encoder, map_encoder2;

   cbor_encoder_init(&encoder, retbuf, cborlen, 0);
   cbor_encoder_create_map(&encoder, &map_encoder, 3);
   cbor_encode_text_stringz(&map_encoder, "type");
   cbor_encode_text_stringz(&map_encoder, "m.login.password");
   cbor_encode_text_stringz(&map_encoder, "identifier");

   cbor_encoder_create_map(&map_encoder, &map_encoder2, 2);
   cbor_encode_text_stringz(&map_encoder2, "type");
   cbor_encode_text_stringz(&map_encoder2, "m.id.user");
   cbor_encode_text_stringz(&map_encoder2, "user");
   CborError err = cbor_encode_text_string(&map_encoder2, username, strlen(username));
   if (err) {
      DEBUG("[CBOR] cannot encode username\n");
      return(-1);
   }
   err = cbor_encoder_close_container(&map_encoder, &map_encoder2);
   if (err) {
      DEBUG("[CBOR] cannot encode identifier\n");
      return(-1);
   }
   cbor_encode_text_stringz(&map_encoder, "password");
   err = cbor_encode_text_string(&map_encoder, password, strlen(password));
   if (err) {
      DEBUG("[CBOR] cannot encode password\n");
      return(-1);
   }
   err = cbor_encoder_close_container(&encoder, &map_encoder);
   if (err) {
      DEBUG("[CBOR] encoding error ocurred\n");
      return(-1);
   }
   else{
      return(cbor_encoder_get_buffer_size(&encoder, retbuf));
   }
}

/**
 * @brief encode payload of comatrix_register request
 * JSON example credentials '{"username":"' + username + '", "password":"' + password + '", "auth": {"type":"m.login.dummy"}}
 *
 *  @param [out] retbuf       Payload buffer for encoded cbor
 *  @param [in] username      Matrix username
 *  @param [in] password      Matrix password
 *  @param [in] cborlen       Length of buffer
 *
 *  @return length of CBOR encoded payload
 *  @return -1 on CBOR encoding error
 */
int encbor_comatrixregister(uint8_t *retbuf, char *username, char *password, size_t cborlen) {
   CborEncoder encoder, map_encoder, map_encoder2;

   cbor_encoder_init(&encoder, retbuf, cborlen, 0);
   cbor_encoder_create_map(&encoder, &map_encoder, 3);
   cbor_encode_text_stringz(&map_encoder, "username");
   CborError err = cbor_encode_text_string(&map_encoder, username, strlen(username));
   if (err) {
      DEBUG("[CBOR] cannot encode username\n");
      return(-1);
   }
   cbor_encode_text_stringz(&map_encoder, "password");
   err = cbor_encode_text_string(&map_encoder, password, strlen(password));
   if (err) {
      DEBUG("[CBOR] cannot encode password\n");
      return(-1);
   }
   cbor_encode_text_stringz(&map_encoder, "auth");

   err = cbor_encoder_create_map(&map_encoder, &map_encoder2, 1);
   cbor_encode_text_stringz(&map_encoder2, "type");
   cbor_encode_text_stringz(&map_encoder2, "m.login.dummy");
   err = cbor_encoder_close_container(&map_encoder, &map_encoder2);
   err = cbor_encoder_close_container(&encoder, &map_encoder);
   if (err) {
      DEBUG("[CBOR] encoding error ocurred\n");
      return(-1);
   }
   else{
      return(cbor_encoder_get_buffer_size(&encoder, retbuf));
   }
}

/**
 *
 *  @ brief decode comatrix_login response payload and set new access token in comatrix state
 *  JSON example token {'access_token':'MDAxZWxvY2F0aW9uIG1hdHJpeC5sb2NhbGhvc3QKMDAxM2lkZW50aWZpZXIga2V5CjAwMTBjaWQgZ2VuID0gMQowMDJjY2lkIHVzZXJfaWQgPSBAZXhhbXBsZTptYXRyaXgubG9jYWxob3N0CjAwMTZjaWQgdHlwZSA9IGFjY2VzcwowMDIxY2lkIG5vbmNlID0gam0qeVlsOkMsa1dlU1NBSwowMDJmc2lnbmF0dXJlIFsVlpt0Ml0jCco_WDxhULeyoq_9vnqnd8l4gxJNYJAiCg'}
 *
 *  @param[out] _cstate       Message buffer for encoded cbor
 *  @param[in] payload        CBOR encoded Synapse token
 *  @param[in] payload_len    Length of CBOR encoded payload
 *
 *  @return length of token
 *  @return 0 if Synapse token is too long for buffer
 *  @return -1 on CBOR decoding error
 */

int decbor_comatrixtoken(comatrix_state *_cstate, uint8_t *payload, size_t payload_len) {
   CborParser parser;
   CborValue  it;
   CborError  err = cbor_parser_init(payload, payload_len, 0, &parser, &it);

   if (cbor_value_is_map(&it) & (err == CborNoError)) {
      CborValue tok;
      size_t    len;
      err = cbor_value_map_find_value(&it, "access_token", &tok);
      err = cbor_value_calculate_string_length(&tok, &len);
      if (len > CONFIG_COMATRIX_MAX_TOKEN_LENGTH) {
         DEBUG("Token is too long for comatrix buffer\n");
         return (0);
      }
      else{
         err = cbor_value_copy_text_string(&tok, &_cstate->comatrix_token[0], &len, &tok);
         _cstate->comatrix_token[len]='\0';
         if (err == CborNoError || err == 257) {
            return(len);
         }
         else{
            DEBUG("[CBOR] decoding error ocurred\n");
            return (-1);
         }
      }
   }

   return(-1);
}

/**
 *
 *  @ brief Decode CoMatrix_join response payload and set new room ID in CoMatrix state
 *  JSON example room_id: {"room_id":"!AHRBprwfaBRKJxOBVW:synapse.host"}
 *
 *  @param [out] _cstate      Message buffer for encoded CBOR
 *  @param [in] payload       Cbor encoded token
 *  @param [in] payload_len   Length of CBOR encoded payload
 *
 *  @return length of Matrix room ID or
 *  @return 0 if Matrix room ID is too long for buffer
 *  @return -1 on CBOR decoding error
 */

int decbor_comatrixroomid(comatrix_state *_cstate, uint8_t *payload, size_t payload_len) {
   CborParser parser;
   CborValue  it;
   CborError  err = cbor_parser_init(payload, payload_len, 0, &parser, &it);

   if (cbor_value_is_map(&it) & (err == CborNoError)) {
      CborValue tok;
      size_t    len;
      err = cbor_value_map_find_value(&it, "room_id", &tok);
      err = cbor_value_calculate_string_length(&tok, &len);
      if (len > CONFIG_COMATRIX_MAX_ROOMID_LENGTH) {
         DEBUG("RoomID is too long for comatrix buffer");
         return (0);
      }
      else{
         err = cbor_value_copy_text_string(&tok, &_cstate->comatrix_roomid[0], &len, &tok);
         _cstate->comatrix_roomid[len]='\0';
         if (err == CborNoError || err == 257) {
            return(len);
         }
         else{
            DEBUG("[CBOR] decoding error occurred");
            return(-1);
         }
      }
   }

   return(-1);
}

/**
 * @brief decode comatrix_recmsg response payload
 * JSON example msg: {'sender': '@test_account:synapse.host', 'body': 'sendmsg test27.04.2021we'}
 *
 *  @param[out] msg           Message struct contains sender and text
 *  @param[in] payload        Cbor encoded message
 *  @param[in] payload_len    Length of cbor encoded payload
 *
 *  @return length of text
 *  @return -1 on CBOR decoding error
 */
int decbor_comatrixmsg(comatrix_msg *msg, uint8_t *payload, size_t payload_len) {
   CborParser parser;
   CborValue  it;
   CborError  err = cbor_parser_init(payload, payload_len, 0, &parser, &it);

   if (cbor_value_is_map(&it) & (err == CborNoError)) {
      CborValue next;
      size_t    len;
      err = cbor_value_map_find_value(&it, "sender", &next);
      err = cbor_value_calculate_string_length(&next, &len);
      msg->sender_len = len + 1;
      err             = cbor_value_copy_text_string(&next, &msg->sender[0], &msg->sender_len, &next);
      //msg->sender[len]="\0";
      err = cbor_value_map_find_value(&it, "body", &next);
      err = cbor_value_calculate_string_length(&next, &len);
      //char text [len + 1];
      msg->text_len = len + 1;
      err           = cbor_value_copy_text_string(&next, &msg->text[0], &msg->text_len, &next);

      if (err == CborNoError || err == 257) {
         return(len);
      }
      else{
         DEBUG("[CBOR] decoding error occurred");
         return(-1);
      }
   }
   else{
      return(-1);
   }
   return(1);
}
