# CoMatrix Client Implementation

## Content

This directory contains the CoMatrix client library and 2 example applications:

* **comatrix** - The implementation of the CoMatrix client library as an external RIOT OS module
* **example comatrix chat** - Provides a CLI client to test all implemented CoMatrix functions via command line
* **example comatrix tempsensor** - Provides an example usage for a sensor node that sends temperature data into a preconfigured Matrix room

## Usage

Copy the Client directory into `(RIOTBASE)/examples/` folder or change the `(RIOTBASE)` variable in the specific Makefile to the absolute path to the RIOT base directory.
