/*
 *
 * CoMatrix is free software and this file is published under the GPLv3 license as
 * described in the accompanying LICENSE file.
 * This Software is for educational purposes only, do not use in production environments.
 *
 * @ingroup     example comatrix tempsensor
 * @{
 *
 * @file        main.c
 * @brief       measures temperature values and sends them into a preconfigured Matrix room
 *
 * @author      ines - ines.kramer@fh-campuswien.ac.at
 * @author      tobi - tobias.buchberger@fh-campuswien.ac.at
 *
 * @}
 */

#include <stdio.h>
#include <stdlib.h>
#include "msg.h"
#include "net/gcoap.h"
#include "shell.h"
#include "../comatrix/include/comatrix.h"
#include "thread.h"
#include "timex.h"
#include "xtimer.h"
#include "mutex.h"
#include "ds18.h"
#include "ds18_params.h"
#include "debug.h"

#define BUFLEN                   (64U)
#define MAIN_QUEUE_SIZE          (4)
#define MODULE_DS18_OPTIMIZED    1
#define MSGBUF_LEN               (20)

static msg_t _main_msg_queue[MAIN_QUEUE_SIZE];
static char  line_buf[SHELL_DEFAULT_BUFSIZE];

static ds18_t  ds18;
static mutex_t sendtemp_lock = MUTEX_INIT;
static char    sendtemp_stack[THREAD_STACKSIZE_MAIN];
static char    msg [MSGBUF_LEN];

/**
 *  @brief thread reads temperature value every minute and sends it to a preconfigured matrix room
 *  stop and start thread using the mutex
 *
 *  @return 0 should never be reached
 */
static void *sendtemp_thread(void *arg) {
   (void)arg;

   while (1) {
      /* Acquire the mutex */
      mutex_lock(&sendtemp_lock);
      int16_t temp = 0;
      int err = ds18_get_temperature(&ds18, &temp);
      if (err == -1) {
         DEBUG("DS18 reading temperature error\n");
      }
      else{
         int le = snprintf(msg, MSGBUF_LEN, "Temp: %i.%u C\n", (temp / 10), (temp % 10));
         /* send temperature into matrix room */
         int len = comatrix_sendmsg(msg, le,0);
                   if (len > 0) {
            printf("Sent Msg: %sMsg length: %d, packet length: %d.\n", msg, le,len);

         }
         else{
            puts("Sending temperature failed.\n");
         }
      }
      /* Release the mutex */
      mutex_unlock(&sendtemp_lock);
      xtimer_sleep(60);
   }

   return(0);
}

static void _sendtemp_thread_usage(char *cmd) {
   printf("usage: %s <start|stop>\n", cmd);
}

/**
 *  @brief handle command line arguments for sendtemp command
 *
 *  @param argc [in] - number of command line arguments
 *  @param argv [in] - commandline arguments
 *
 *  @return 0 if command is executed -1 when the command usage is printed
 */
static int sendtemp_thread_handler(int argc, char *argv[]) {
   (void)argc;
   if (!strcmp(argv[1], "start")) {
      mutex_unlock(&sendtemp_lock);
   }
   else if (!strcmp(argv[1], "stop")) {
      mutex_trylock(&sendtemp_lock);
   }
   else{
      _sendtemp_thread_usage(argv[0]);
      return(-1);
   }
   return(0);
}

/**
 *  @brief executes readtemp command and prints the Temperature
 *  stop and start thread using the mutex
 *
 *  @return 0 on execution -1 in case of an error
 */
static int read_temp(int argc, char *argv[]) {
   (void)argc;
   (void)argv;
   int16_t temp = 0;
   int err = ds18_get_temperature(&ds18, &temp);
   if (err == -1) {
      printf("DS18 reading temperature error\n");
      return(-1);
   }
   else{
      printf("Temp: %i.%u C\n", (temp / 10), (temp % 10));
   }
   return(0);
}

/* enables comatrix_cli commands */
static const shell_command_t shell_commands[] = {
   { "readtemp", "read DS18 temperature", read_temp },
   { "sendtemp", "continuously read and send temperature to a matrix room", sendtemp_thread_handler },
   { NULL,       NULL,                                                   NULL                    }
};

int main(void) {

   /* initialize ds18 sensor */
   int err = ds18_init(&ds18, ds18_params);
   if (err == DS18_OK) {
      printf("DS18 initialized successfully\n");
   }
   else if (err == DS18_ERROR) {
      printf("DS18 initialization error\n");
   }

   /* init comatrix client state */
   comatrix_init(0);

   /* start thread which reads and sends continuously temperature values */
   thread_create(sendtemp_stack, sizeof(sendtemp_stack), THREAD_PRIORITY_MAIN - 1,
                 0, sendtemp_thread, NULL, "sendtemp");

   /* thread running the shell */
   msg_init_queue(_main_msg_queue, MAIN_QUEUE_SIZE);

   shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);

   /* should never be reached */
   return(0);
}
