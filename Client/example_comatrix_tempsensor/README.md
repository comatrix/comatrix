# Example CoMatrix Temperature Sensor

A prototype application, which illustrates a simple use case of a temperature sensor node (SAMR21-xpro + DS18S20) that sends temperature data into a pre-configured Matrix room using the CoMatrix client library.

## Hardware:

* A DS18S20 temperature sensor and a 4,7 kohm resistor
* A SAMR21-xpro

#### DS18S20 temperature sensor

![](https://i.imgur.com/MyFxYn6.png)

* Datasheet: https://datasheets.maximintegrated.com/en/ds/DS18S20.pdf

#### SAMR21-xpro

* Datasheet: http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42243-SAMR21-Xplained-Pro_User-Guide.pdf

![](https://i.imgur.com/iaAKLMu.png)

**Important**: The DQ of DS18S20 sensor needs to be connected to a GPIO Pin, in our case we used PA14 and over a 4,7 kohm resistor to the VCC on the SAMR21-xpro.

**Connect Pinout:**

```
DS18S20                          4,7 kohm resistor           SAMR21-xpro
GND (left pin of DS18S20)    ->         ->             ->   GND  (Ext3 19 on SAMR21-xpro)
DQ  (middle pin of DS18S20)  ->  4,7 kohm (left pin)   ->   PA14 (Ext3 15 on SAMR21-xpro)
VDD (right pin of DS18S20)    ->  4,7 kohm (right pin)  ->   VCC
```

## Preliminaries

* **Tested environment**
  * Code tested on a SAMR21-xpro microcontroller with RIOT OS (Release-2021.04,tag 46bc55f514).
  * Tested toolchain: GNU Arm Embedded Toolchain 9-2020-q2-update
* **Setup**
  * Reqirements: Running CoMatrix Gateway and Matrix-Synapse homeserver (Follow the steps in the [SETUP.md](../../SETUP.md) tutorial)
  * Copy the Client folder into the `RIOT/example/` folder of your RIOT base directory.

## Client Configuration

The application specific settings need to be set in an `app.config` file, consider the `app.config.example`.

* Synapse endpoint (e.g. `CONFIG_COMATRIX_SYNAPSE="http://192.168.1.1:8008"`)
* Gateway link-local IPv6 address (e.g. `CONFIG_COMATRIX_GATEWAYIP6="fe80::1"`)
* Matrix room ID (e.g. `CONFIG_COMATRIX_ROOMID="!kYsGiNgnRRVZCizHAF:synapse"`)
* Synapse user specific access token (e.g. `CONFIG_COMATRIX_TOKEN="MDAxNWxvY..."`)
* Default 6LoWPAN and WPAN settings:
   * default WPAN channel: `26`
   * default 6LoWPAN PANID: `0x23`

## Compilation

Change to the application root folder (`RIOTBASE/example/Client/example_comatrix_tempsensor`) and build the application via `make` for the samr21-xpro board and flash the binary firmware on the microcontroller:

```
make BOARD=samr21-xpro
make BOARD=samr21-xpro flash
```

Enable serial communication over an USB cable between SAMR21-xpro and a PC using `pyterm` (`python -m pip install pyterm`) and connect to the SAMR21-xpro:

```
make BOARD=samr21-xpro term
```

## Usage

If valid parameters are set (Synapse access token, Matrix room ID and Matrix-Synapse homeserver endpoint and CoMatrix gateway address) the application will send temperature values into the specified Matrix room each minute, immediately after starting. Stop sending messages with the `sendtemp stop` command.

Available application specific commands are:

* **readtemp**: Reads and prints temperature value from DS18S20 temperature sensor.

```
usage: readtemp
```

* **sendtemp**: Start and stop the thread that continuously sends temperature values into the configured Matrix room.

```
usage: sendtemp <start|stop>
```

## Future work

* Enable encryption of the communication to the gateway either via DTLS support, OSCORE or OpenThread.
* Modify sendtemp thread that only on temperature change a message is sent into a room (these messages are so annoying:)).
