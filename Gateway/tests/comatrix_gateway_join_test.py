#!/usr/bin/env python3

# This file is part of the CoMatrix project.
#
# This script tests the joining of a Matrix room after a user received an invitation from another user, by sending a
# CoAP request to a CoMatrix gateway.
#
# 2021 Tobi Buchberger <tobias.buchberger@fh-campuswien.ac.at>
# 2021 Ines Kramer <ines.kramer@fh-campuswien.ac.at>
#
# CoMatrix is free software, this file is published under the GPLv3 license as
# described in the accompanying LICENSE file.
# This Software is for educational purposes only, do not use in production environments.

import logging
import asyncio

from aiocoap import *

CONTENT_FORMAT_CBOR = 60
CONTENT_FORMAT_TEXT_PLAIN = 0
CONFIRMABLE = 0
NON_CONFIRMABLE = 1
# TODO set access token which should be used in tests
ACCESS_TOKEN = ''

logging.basicConfig(level=logging.INFO)


async def main():

    context = await Context.create_client_context()

    # according to matrix CS-API spec, `from=` is required in the URI, but Synapse accepts the request without
    # TODO test short URL, set correct uri and proxy_uri
    request = Message(code=POST, mtype=CONFIRMABLE, uri="coap://localhost/join",
                      proxy_uri="http://localhost:8008/K/<room_id>")
    # TODO test full URL, set correct uri and proxy_uri
    request = Message(code=POST, mtype=CONFIRMABLE, uri="coap://localhost/join",
                      proxy_uri="http://localhost:8008/_matrix/client/r0/rooms/<room_id>/join")
    access_token_option = optiontypes.StringOption(256, ACCESS_TOKEN)
    request.opt.add_option(access_token_option)

    response = await context.request(request).response

    print('########################')
    print('Result: %s' % response.code)
    # print('Response Payload (JSON): %s' % loads(response.payload))

    # TODO test time resource, set correct uri
    # request = Message(code=GET, mtype=CONFIRMABLE, uri="coap://localhost/time",)
    # response = await context.request(request).response
    #
    # print('########################')
    # print('Result: %s' % (response.code))
    # print('Current Unix timestamp: %s' % response.payload)


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
