#!/usr/bin/env python3

# This file is part of the CoMatrix project.
#
# This script tests the login of an existing Matrix-Synapse user by sending a CoAP request to a CoMatrix gateway.
#
# 2021 Tobi Buchberger <tobias.buchberger@fh-campuswien.ac.at>
# 2021 Ines Kramer <ines.kramer@fh-campuswien.ac.at>
#
# CoMatrix is free software, this file is published under the GPLv3 license as
# described in the accompanying LICENSE file.
# This Software is for educational purposes only, do not use in production environments.

import logging
import asyncio

import aiocoap
from aiocoap import *
from cbor2 import dumps, loads
import json

CONTENT_FORMAT_CBOR = 60
CONTENT_FORMAT_TEXT_PLAIN = 0
NON_CONFIRMABLE = 1
CONFIRMABLE = 0

logging.basicConfig(level=logging.INFO)


async def main():
    context = await Context.create_client_context()

    # TODO set correct username and password
    payload = '{"type":"m.login.password", "identifier": {"type":"m.id.user", "user":"<username>"}, "password":"<password>"}'
    print('Payload (String): %s' % payload)
    payload_json = json.loads(payload)
    print('Payload (JSON): %s' % payload_json)
    payload_cbor = dumps(payload_json)
    print('Payload (CBOR): %s' % payload_cbor)

    # TODO to test short URL, set correct uri and proxy_uri
    request = Message(code=aiocoap.Code.POST, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR,
                      mtype=NON_CONFIRMABLE,
                      uri="coap://localhost/login",
                      proxy_uri="http://localhost:8008/1")
    # TODO to test full URL, set correct uri and proxy_uri
    # request = Message(code=aiocoap.Code.POST, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR,
    #                  mtype=NON_CONFIRMABLE,
    #                  uri="coap://localhost/login",
    #                  proxy_uri="http://localhost:8008/_matrix/client/r0/login")

    response = await context.request(request).response

    print('########################')
    print('Result: %s' % response.code)
    print('Response Payload (JSON): %s' % loads(response.payload))

if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
