#!/usr/bin/env python3

# This file is part of the CoMatrix project.
#
# This script generates a random username and password (=username). This user is then registered at a Matrix-Synapse HS
# by sending a CoAP request to a CoMatrix gateway. The generated access token for this user (after successful
# registration) is then used to test the LogoutResource of the CoMatrix gateway.
#
# 2021 Tobi Buchberger <tobias.buchberger@fh-campuswien.ac.at>
# 2021 Ines Kramer <ines.kramer@fh-campuswien.ac.at>
#
# CoMatrix is free software, this file is published under the GPLv3 license as
# described in the accompanying LICENSE file.
# This Software is for educational purposes only, do not use in production environments.

import logging
import asyncio
import random

import aiocoap
from aiocoap import *
from cbor2 import dumps, loads
import json

CONTENT_FORMAT_CBOR = 60
CONTENT_FORMAT_TEXT_PLAIN = 0
NON_CONFIRMABLE = 1
CONFIRMABLE = 0

logging.basicConfig(level=logging.INFO)


async def main():
    context = await Context.create_client_context()

    # generate random username
    username = "test" + str(random.randint(1000, 9999))
    password = username
    payload = '{"username":"' + username + '", "password":"' + password + '", "auth": {"type":"m.login.dummy"}}'
    # payload = '{"username":"' + username + '", "password":"' + password + '", "auth": {"type":"m.login.password"}}'
    # payload = '{"username":"' + username + '", "auth": {"type":"m.login.dummy"}}'
    # payload = '{"username":"' + username + '", "password":"' + password + '"}'
    print('Payload (String): %s' % payload)
    payload_json = json.loads(payload)
    print('Payload (JSON): %s' % payload_json)
    payload_cbor = dumps(payload_json)
    print('Payload (CBOR): %s' % payload_cbor)

    # TODO to test short URL, set correct uri and proxy_uri
    # request = Message(code=aiocoap.Code.POST, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR, mtype=CONFIRMABLE,
    #                   uri="coap://localhost/register",
    #                   proxy_uri="http://localhost:8008/4")
    # TODO to test full URL, set correct uri and proxy_uri
    request = Message(code=aiocoap.Code.POST, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR,
                      mtype=CONFIRMABLE,
                      uri="coap://localhost/register",
                      proxy_uri="http://localhost:8008/_matrix/client/r0/register")
    # TODO test missing proxy_uri, set correct uri
    # request = Message(code=aiocoap.Code.POST, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR,
    #                   mtype=CONFIRMABLE,
    #                   uri="coap://localhost/register")

    response = await context.request(request).response

    print('########################')
    print('Result: %s' % (response.code))
    response_json = loads(response.payload)
    print('Response Payload (JSON): %s' % response_json)
    access_token = response_json['access_token']

    """Test LogoutResource with newly created user"""

    # TODO to test short URL, set correct uri and proxy_uri
    # request = Message(code=aiocoap.Code.POST, uri="coap://localhost/logout",
    #                   proxy_uri="http://localhost:8008/3")
    # TODO to test full URL, set correct uri and proxy_uri
    request = Message(code=aiocoap.Code.POST, mtype=CONFIRMABLE, uri="coap://localhost/logout",
                      proxy_uri="http://localhost:8008/_matrix/client/r0/logout/all")
    access_token_option = optiontypes.StringOption(256, access_token)
    request.opt.add_option(access_token_option)

    response = await context.request(request).response

    print('########################')
    print('Logout Result: %s' % response.code)


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
