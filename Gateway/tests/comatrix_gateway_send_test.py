#!/usr/bin/env python3

# This file is part of the CoMatrix project.
#
# This script tests the sending of a new message to a Matrix room by sending a CoAP request to a CoMatrix gateway.
#
# 2021 Tobi Buchberger <tobias.buchberger@fh-campuswien.ac.at>
# 2021 Ines Kramer <ines.kramer@fh-campuswien.ac.at>
#
# CoMatrix is free software, this file is published under the GPLv3 license as
# described in the accompanying LICENSE file.
# This Software is for educational purposes only, do not use in production environments.

import logging
import asyncio

from aiocoap import *
from cbor2 import dumps, loads
import json
# to generate random temperature value
from random import randrange
# To generate unique txnId based on current timestamp
import time

CONTENT_FORMAT_CBOR = 60
CONTENT_FORMAT_TEXT_PLAIN = 0
NON_CONFIRMABLE = 1
CONFIRMABLE = 0
# TODO set access token which should be used in tests
ACCESS_TOKEN = ''

logging.basicConfig(level=logging.INFO)


async def main():
    context = await Context.create_client_context()

    payload = '{"msgtype":"m.text","body":"Temp: ' + str(randrange(30)) + '.' + str(randrange(9)) + '"}'
    print('Payload (String): %s' % payload)
    payload_json = json.loads(payload)
    print('Payload (JSON): %s' % payload_json)
    payload_cbor = dumps(payload_json)
    print('Payload (CBOR): %s' % payload_cbor)

    # TODO txnId based on current timestamp
    txnId = str(int(time.time()))
    print(txnId)
    # TODO test short URL, set correct uri and proxy_uri
    request = Message(code=PUT, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR, mtype=NON_CONFIRMABLE,
                      uri="coap://localhost/send",
                      proxy_uri="http://localhost:8008/9/<room_id>/m.room.message/" + txnId)
    # TODO test full URL, set correct uri and proxy_uri
    # request = Message(code=PUT, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR, mtype=NON_CONFIRMABLE, uri="coap://localhost/send",
    #    proxy_uri="http://localhost:8008/_matrix/client/r0/rooms/<room_id>/send/m.room.message")

    # TODO txnId added from proxy, set correct uri and proxy_uri
    # request = Message(code=PUT, payload=payload_cbor, content_format=CONTENT_FORMAT_CBOR, mtype=NON_CONFIRMABLE,
    #                   uri="coap://localhost/sendproxy",
    #                   proxy_uri="http://localhost:8008/9/<room_id>/m.room.message")

    access_token_option = optiontypes.StringOption(256, ACCESS_TOKEN)
    request.opt.add_option(access_token_option)

    response = await context.request(request).response

    print('########################')
    print('Result: %s' % (response.code))
    if response.payload != b'':
        print('Response Payload (JSON): %s' % loads(response.payload))


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(main())
