# CoMatrix - Constrained Matrix

<img src="resources/comatrix.png" alt="CoMatrix" width="50%"/>

The CoMatrix project enables the usage of the Matrix protocol (more precisely Matrix Client-Server API [0]) for 
constrained IoT devices via CoAP [1] and CBOR [2] in a constrained network (e.g. a 802.15.4/6LoWPAN network [3]).

The architecture of the Matrix protocol is based on HTTP/REST/JSON/TLS and is therefore not suitable for constrained devices.
CoMatrix provides a gateway, which ports Matrix to CoAP/CBOR/(DTLS). This gateway communicates with constrained IoT 
devices on one side via CoAP+CBOR and translates to the Matrix protocol on the other side (i.e. 
HTTP+JSON). We also provide a CoMatrix client library (for RIOT-OS [4]) which provides a starting point to implement CoAP clients (for constrained devices)
which are able to interact with Matrix homeservers via the gateway.

As far as we know there were no practical implementations or use of Matrix on constrained IoT devices before CoMatrix. 
CoMatrix now provides the basis to integrate Matrix-based communications in your IoT project(s). 
It enables resource-sparing communications for devices where this is necessary (e.g. microcontrollers, SoCs), 
but it also enables reliable communications for/with non-constrained devices (i.e. PCs, servers, laptops, smartphones) via standard Matrix implementations.
(Co)Matrix may evolve into an alternative to MQTT/MQTT-SN.

**Currently CoMatrix supports the following features:**
- Sending of messages to a Matrix room
- Receiving of the last message of a Matrix room
- User registration at a Matrix Synapse HS
- Joining of a Matrix room upon invitation
- Login of an user at a Matrix Synapse HS
- Logout of an user at a Matrix Synapse HS

**Target audience:**
- Matrix/Open-Source Community
- University staff / researchers
- Developers
- Makers

**Important links:**

- Project website: https://comatrix.eu/
- Project blog: https://www.netidee.at/comatrix
- [The Matrix & The CoMatrix - Sichere und dezentrale Kommunikation für Menschen und Maschinen (Linuxwochen Wien 2019, 04.05.2019)](resources/comatrix.pdf) (presentation of the concept behind the project)
- [CoMatrix - How to overcome kernel panic, deal with buffer limitations and LOL at random (RIOT Summit 2021, 10.09.2021)](resources/CoMatrix_RIOT_Summit_2021.pdf) (presentation of the project results)
    - https://summit.riot-os.org/2021/blog/speakers/tobias-buchberger/
- [CoMatrix - Constrained Matrix (rC3 2021, 27.12.2021)](resources/CoMatrix_rC3_2021.pdf) (presentation of the project results)
    - https://cfp.franconian.net/end-of-year-event-2021/talk/UB8LL3/

Talk recording of our CoMatrix presentation at RIOT Summit 2021:

[![CoMatrix presentation @ RIOT Summit 2021](https://img.youtube.com/vi/TG358u6KjSk/0.jpg)](https://www.youtube.com/watch?v=TG358u6KjSk&t=196s "CoMatrix presentation @ RIOT Summit 2021")



## CoMatrix Architecture & Testbed

CoMatrix provides:
- A **CoMatrix gateway**: a CoAP server/HTTP forward proxy software based on aiocoap [5]. This gateway is intended to be 
  deployed on a border router (e.g. a Raspberry Pi). 
  It communicates with CoMatrix clients via the "CoMatrix protocol" and translates incoming CoAP requests to HTTP requests conforming with
  the Matrix CS-API. These HTTP requests are then sent to a certain Matrix-Synapse homeserver. See the [Gateway-README](Gateway/README.md) for details.
- A **CoMatrix client library**: a RIOT-OS software module which enables CoMatrix communication for constrained IoT devices (e.g. a 
  SAMR21-xpro microcontroller) via a constrained network (e.g. 802.15.4+6LoWPAN). See the [Client-README](Client/README.md) for details.

![CoMatrix Architecture & Testbed](Architecture/CoMatrix_Architecture_Final.png "CoMatrix Architecture & Testbed")

**See [SETUP.md](SETUP.md) for details about our testbed setup.**

### Testbed hardware

Our testbed was built by using the following hardware:

- CoMatrix Gateway:
  - Raspberry Pi 3B(+)
      - https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/
  - OpenLabs Raspberry Pi 802.15.4 radio module
    - https://openlabs.co/store/Raspberry-Pi-802.15.4-radio
    - **CAUTION**: In our tests the module was not able to successfully communicate when used with a Raspberry Pi 4 and 
      current Raspbian/Raspberry Pi OS (i.e. `Linux raspberrypi 5.10.17-v7+`)! We recommend to use a Raspberry Pi 3B(+).
- CoMatrix Client:
    - Atmel SAM R21 Xplained Pro Evaluation Kit (SAMR21-xpro) microcontroller
        - https://www.microchip.com/developmenttools/ProductDetails/ATSAMR21-XPRO
    - Temperature sensor "DS18S20+"
        - https://at.farnell.com/maxim-integrated-products/ds18s20/digital-thermometer-18s20-to92/dp/2519401?st=Maxim%20DS18S20
- Matrix Synapse homeserver:
    - Raspberry Pi 4
- A laptop (running Matrix Element client)
  - OPTIONAL: Nordic nRF52840 Dongle for monitoring/sniffing of 802.15.4 network traffic
        - https://www.nordicsemi.com/Software-and-tools/Development-Kits/nRF52840-Dongle

![Real Testbed](resources/SAMR21xpro-Raspberry_Pi_3B+_1.png "Real Testbed")

## CoMatrix IoT scenarios / use cases

- **Scenario #1 "Wireless Sensor Network"**: 
  - Multiple constrained IoT devices with a sensor (or multiple sensors) are sending sensor data 
  to a Matrix room (implemented in [the tempsensor example](Client/example_comatrix_tempsensor)). For example to collect the 
  temperature of different rooms in a building.
- **Scenario #2 "Actuator"**: 
  - A constrained IoT device is acting after receiving a (specific) message via a Matrix room. For example an IoT 
  device with a connected monitor/display that displays the latest message of a Matrix room.
  - A constrained IoT device is acting based on sensor data sent to a Matrix room.
- **Scenario #3 "Smart Home"**: 
  - < Insert your ideas >

## CoMatrix in comparison to "MSC3079: Low Bandwidth Client-Server API"

While working on our CoMatrix project, "MSC3079: Low Bandwidth Client-Server API" was publicly announced on 01.04.2021.
When implemented, this would enable CoAP clients to directly send requests to CoAP endpoints provided by a Matrix homeserver (e.g. Synapse).
Homeservers would then provide a CoAP/CBOR alternative to the existing HTTP/JSON API, but will not replace the existing HTTP/JSON APIs. 
The CoMatrix project in comparison enables CoMatrix clients to communicate with a Matrix homeserver (i.e. Synapse) via CoAP/CBOR without requiring the homeserver
to provide CoAP endpoints. 

Moreover, our CoMatrix client library (for RIOT-OS) can be used as a starting point to implement CoAP clients (for constrained devices)
which are able to send CoAP requests to  Matrix homeservers directly (after MSC3079 is implemented in e.g. Synapse).

However, we also implemented a few design proposals from MSC3079 in CoMatrix:
- MSC3079 specifies single character path enums which act as shorthand for certain commonly used CS-API paths. We used this
"short URLs" in our CoMatrix gateway implementation to optionally be used by clients to reduce the necessary number of bytes
for information included in the "Proxy-Uri" CoAP option.
- CoAP defines many mappings, but notably has no Option mapping for Authorization headers. The MSC3079 proposal therefore 
  marks the option ID 256 as `Authorization` in accordance with the CoAP Option Registry 
(options IDs 0-255 are reserved for IETF review). We also require the usage of CoAP option ID 256 to include access tokens
  in CoAP requests sent by CoMatrix clients. In the CoMatrix case, the access tokens need to be included in every 
request and can't be omitted on subsequent requests.

Also, our gateway allows to (optionally) define a hardcoded access token to be used for CS-API requests and to (optionally) 
add a `txnId` for sending a message to a Matrix room. This reduces the bandwidth consumed by CoMatrix. 

**Links/resources:**

- MSC3079 blog announcement: https://matrix.org/blog/2021/04/01/this-week-in-matrix-2021-04-01#msc-status
- MSC3079 Pull request: https://github.com/matrix-org/matrix-doc/pull/3079
- MSC3079 Markdown file: https://github.com/matrix-org/matrix-doc/blob/5e689f00f6953d15dcfcfcc519969e0a460bc6c9/proposals/3079-low-bandwidth-csapi.md

### CoMatrix in comparison to "coap-proxy"

> coap-proxy is a proof of concept experiment for converting normal Matrix HTTPS+JSON traffic into an 
> ultra-low-bandwidth CoAP+CBOR+Flate+Noise+UDP network transport. The resulting transport typically 
> uses 35x-70x less bandwidth than HTTPS+JSON, and attempts to fit typical Matrix transactions into a 
> single roundtrip on a 100bps network link.

Therefore _coap-proxy_ has a different use case. It converts normal Matrix HTTPS+JSON traffic into CoAP+CBOR. CoMatrix
enables CoAP+CBOR based communication from contrained devices to the CoMatrix gateway. The gateway then translates 
CoMatrix messages to normal Matrix HTTPS+JSON. 

**Link:** https://github.com/matrix-org/coap-proxy

## Limitations

- CoMatrix messages are currently exchanged in plaintext CoAP between clients and the gateway (i.e. in the IEEE 802.15.4 
  6LoWPAN network)! Transport security will be added in [future work](#future-work).
- CoAP block-wise transfers (RFC7959) are not implemented yet in the CoMatrix client library. Therefore messages should be kept as 
  small as possible.
  - cf. https://datatracker.ietf.org/doc/html/rfc7959
  - FYI: We successfully tested CoMatrix with messages consisting of multiple 802.15.4 frames (without using block-wise transfers).
- Synapse's access tokens currently use bandwidth on every request if there is no hardcoded access token provided on 
  the gateway. 
  - ~~In the future, Synapse's access token implementation may be adapted to use shorter (random) strings instead of macaroons~~
(cf. [6]).
    - The new access token format is already implemented in Synapse v1.34! https://matrix.org/blog/2021/05/17/synapse-1-34-0-released
    

## Future Work

- Transport security (e.g. DTLS [7] / OSCORE [8] / OpenThread [9])
- Comparison of CoMatrix to other IoT protocols like MQTT(-SN)

For more information on possible future work see the corresponding sections in [Gateway/README.md](Gateway/README.md#future-work)
and [Client/comatrix/README.md](Client/comatrix/README.md#future-work).

## Resources

- [0] https://matrix.org/docs/spec/client_server/r0.6.1
- [1] https://coap.technology/
- [2] https://cbor.io/
- [3] https://datatracker.ietf.org/wg/6lowpan/documents/
- [4] https://www.riot-os.org/
- [5] https://github.com/chrysn/aiocoap
- [6] https://github.com/matrix-org/synapse/pull/5588#issuecomment-794260500
- [7] DTLS v1.2 (RFC6347): https://datatracker.ietf.org/doc/html/rfc6347
- [8] OSCORE (RFC8613): https://datatracker.ietf.org/doc/html/rfc8613
- [9] OpenThread: https://openthread.io/

## Acknowledgements

CoMatrix was funded by [Netidee](https://www.netidee.at/).

![Netidee Logo](resources/netidee_logo.jpg "Netidee Logo")

___

Special thanks to @chrysn (Christian Amsüss) and the folks in the RIOT-OS IRC channel/Matrix room for their support.

#### aiocoap acknowledgements

Amsüss, Christian and Wasilak, Maciej. aiocoap: Python CoAP Library. Energy Harvesting Solutions, 2013–. http://github.com/chrysn/aiocoap/

## Licensing

CoMatrix is published under the GPLv3 License, see [LICENSE](LICENSE) for details.

- 2021 Tobi Buchberger <tobias.buchberger@fh-campuswien.ac.at>
- 2021 Ines Kramer <ines.kramer@fh-campuswien.ac.at>
